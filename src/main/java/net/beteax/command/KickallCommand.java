/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.command;

import net.beteax.BeteaxAPI;
import net.beteax.resources.Syntaxes;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class KickallCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
        if (cs instanceof Player) {
            Player player = (Player) cs;
            if (player.hasPermission("lobby.administrator") || player.hasPermission("lobby.headbuilder")
                    || player.hasPermission("lobby.srmoderator") || player.hasPermission("lobby.developer") || player.hasPermission("lobby.kickall")) {
                if(args.length == 1){
                    switch (args[0]){
                        case "others":{
                            for (Player online : Bukkit.getOnlinePlayers()) {
                                if(online != player || !(online.hasPermission("lobby.premium")||online.hasPermission("lobby.administrator") || online.hasPermission("lobby.headbuilder")
                                        || online.hasPermission("lobby.srmoderator") || online.hasPermission("lobby.developer") || online.hasPermission("lobby.kickall"))){
                                    online.kickPlayer("§7Autokick by §e"+ player.getName());
                                }
                            }
                            player.sendMessage(BeteaxAPI.getAPI().getPrefixes().beteax+"§cDu hast alle anderen Spieler runtergworfen.");
                            break;
                        }
                        case "nonpremium":{
                            for (Player online : Bukkit.getOnlinePlayers()) {
                                if(online.hasPermission("lobby.spieler")){
                                    online.kickPlayer("§7Autokick by §e"+ player.getName());
                                }
                            }
                            player.sendMessage(BeteaxAPI.getAPI().getPrefixes().beteax+"§cDu hast alle §enonPremium §cSpieler runtergworfen.");
                            break;
                        }
                        case "premium":{
                            for (Player online : Bukkit.getOnlinePlayers()) {
                                if(online.hasPermission("lobby.premium")){
                                    online.kickPlayer("§7Autokick by §e"+ player.getName());
                                }
                            }
                            player.sendMessage(BeteaxAPI.getAPI().getPrefixes().beteax+"§cDu hast alle §ePremium §cSpieler runtergworfen.");
                            break;
                        }
                        case "otherranks":{
                            for (Player online : Bukkit.getOnlinePlayers()) {
                                if(!(online.hasPermission("lobby.spieler") || online.hasPermission("lobby.youtuber")|| online.hasPermission("lobby.premium")||online.hasPermission("lobby.administrator") || online.hasPermission("lobby.headbuilder")
                                        || online.hasPermission("lobby.srmoderator") || online.hasPermission("lobby.developer") || online.hasPermission("lobby.kickall"))){
                                    online.kickPlayer("§7Autokick by §e"+ player.getName());
                                }
                            }
                            player.sendMessage(BeteaxAPI.getAPI().getPrefixes().beteax+"§cDu hast alle §eanderen Rang §cSpieler runtergworfen.");
                            break;
                        }
                        case "all":{
                            Bukkit.getOnlinePlayers().forEach(on -> on.kickPlayer("§7Autokick by §e"+ player.getName()));
                            break;
                        }
                    }
                }else {
                    Syntaxes.sendSyntax(player, "/kickall [others,all,nonpremium,premium,otherranks]");
                }
            } else {
                Syntaxes.sendNoPermission(player, "/kickall");
            }
        } else {
            Bukkit.getOnlinePlayers().forEach(on -> on.kickPlayer("§7Autokick by §eSERVER"));
        }
        return false;
    }
}
