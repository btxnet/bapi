/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.command;

import net.beteax.BeteaxAPI;
import net.beteax.resources.Syntaxes;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FixCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
                if (args.length == 0) {
                    p.sendMessage(BeteaxAPI.getAPI().getPrefixes().beteax + "§7Deine Position wurde gefixt.");

                    p.teleport(p.getLocation().add(0.0D, 0.2D, 0.0D));
                } else {
                    Syntaxes.sendSyntax(p,"/fix");
                }



        }else {
            Syntaxes.sendNoPlayer(sender);
        }

        return false;
    }
}
