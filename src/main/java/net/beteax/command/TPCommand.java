/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.command;

import net.beteax.BeteaxAPI;
import net.beteax.resources.Syntaxes;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TPCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
        if (cs instanceof Player) {
            Player player = (Player) cs;
            if (player.hasPermission("lobby.administrator") || player.hasPermission("lobby.headbuilder")
                    || player.hasPermission("lobby.srmoderator") || player.hasPermission("lobby.developer") || player.hasPermission("lobby.tp")) {
                if (args.length == 1) {
                    Player target = Bukkit.getPlayerExact(args[0]);
                    if(target != null){
                        player.teleport(target);
                        player.sendMessage(BeteaxAPI.getAPI().getPrefixes().beteax+"§7Du hast dich zu §r"+target.getDisplayName()+"§7 teleportiert.");
                        return false;
                    }else {
                        player.sendMessage(BeteaxAPI.getAPI().getPrefixes().beteax+"§cDieser Spieler ist nicht Online.");
                        return false;
                    }
                }else if(args.length == 2){
                    Player who = Bukkit.getPlayerExact(args[0]);
                    Player target = Bukkit.getPlayerExact(args[1]);
                    if(target != null || who != null){
                        who.teleport(target);
                        player.sendMessage(BeteaxAPI.getAPI().getPrefixes().beteax+"§7Du hast §r"+who.getDisplayName()+"§7 zu §r"+target.getDisplayName()+"§7 teleportiert.");
                        return false;
                    }else {
                        player.sendMessage(BeteaxAPI.getAPI().getPrefixes().beteax+"§cDieser Spieler ist nicht Online.");
                        return false;
                    }
                }else {
                    Syntaxes.sendSyntax(player, "/tp <Player> [Player]");
                }
            } else {
                Syntaxes.sendNoPermission(player, "/tp");
            }
        } else {
            Syntaxes.sendNoPlayer(cs);
        }
        return false;
    }
}
