/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.api;

import net.beteax.APIManager;
import net.beteax.BeteaxAPI;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Coins {

    private APIManager api;
    private BeteaxAPI main;
    public Coins(BeteaxAPI main, APIManager api){
        this.main = main;
        this.api = api;
    }

    /**
     *
     * @param uuid Player Unique ID
     * @return Coins of the Player
     */
    public int get(String uuid)
    {
        ResultSet rs = this.api.getNetworkSQL().getResult("SELECT * FROM UserData WHERE UUID='"+uuid+"'");
        try
        {
            rs.next();
            return rs.getInt("Coins");
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     *
     * @param uuid Player UUID
     * @param value Coins to add
     */
    public void add(String uuid,Integer value)
    {
        int c = get(uuid);
        PreparedStatement p = this.api.getNetworkSQL().getStatement("UPDATE UserData SET Coins=? WHERE UUID=?");
        try
        {
            p.setInt(1,c+value);
            p.setString(2,uuid);
            p.executeUpdate();
            p.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

    }

    /**
     *
     * @param uuid Player UUID
     * @param value Coins to remove
     */
    public void remove(String uuid,Integer value)
    {
        int c = get(uuid);
        PreparedStatement p = this.api.getNetworkSQL().getStatement("UPDATE UserData SET Coins=? WHERE UUID=?");
        try
        {
            p.setInt(1,c-value);
            p.setString(2,uuid);
            p.executeUpdate();
            p.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

    }
}
