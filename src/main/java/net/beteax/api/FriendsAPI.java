/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.api;

import net.beteax.APIManager;
import net.beteax.BeteaxAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class FriendsAPI {
    BeteaxAPI main;
    APIManager api;
    int maxreq = 25;
    int maxfriends = 50;

    public FriendsAPI(BeteaxAPI main, APIManager api) {
        this.main = main;
        this.api = api;
    }
    public int getMaxRequests(){
        return this.maxreq;
    }
    public int getMaxFriends(){
        return this.maxfriends;
    }

    public String getServer(String uuid) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Friends WHERE UUID= ?");
            ps.setString(1, uuid);
            ResultSet rs = ps.executeQuery();
            rs.next();
            String last = rs.getString("CurrentServer");
            rs.close();
            ps.close();
            return last;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "Connecting...";
    }
    private long getLastOnline(String uuid) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Friends WHERE UUID= ?");
            ps.setString(1, uuid);
            ResultSet rs = ps.executeQuery();
            rs.next();
            long last = rs.getLong("LastConnect");
            rs.close();
            ps.close();
            return last;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    public String getLastConnectString(String uuid) {

        long current = System.currentTimeMillis();
        long diff = current - getLastOnline(uuid);
        long seconds = 0;
        long minutes = 0;
        long hours = 0;
        long days = 0;
        long weeks = 0;
        long months = 0;
        long years = 0;
        while (diff > 1000) {
            diff -= 1000;
            seconds++;
        }
        while (seconds > 60) {
            seconds -= 60;
            minutes++;
        }
        while (minutes > 60) {
            minutes -= 60;
            hours++;
        }
        while (hours > 24) {
            hours -= 24;
            days++;
        }
        while (days > 7) {
            days -= 7;
            weeks++;
        }
        while (weeks > 4) {
            weeks -= 4;
            months++;
        }
        while (months > 12) {
            months -= 12;
            years++;
        }
        if (years != 0) {
            if (years == 1) {
                return "§e" + 1 + " §7Jahr";
            } else {
                return "§e" + years + " §7Jahre";
            }
        } else if (months != 0) {
            if (months == 1) {
                return "§e" + 1 + " §7Monat";
            } else {
                return "§e" + months + " §7Monate";
            }
        } else if (weeks != 0) {
            if (weeks == 1) {
                return "§e" + 1 + " §7Woche";
            } else {
                return "§e" + weeks + " §7Wochen";
            }
        } else if (days != 0) {
            if (days == 1) {
                return "§e" + 1 + " §7Tag";
            } else {
                return "§e" + days + " §7Tage";
            }
        } else if (hours != 0) {
            if (hours == 1) {
                return "§e" + 1 + " §7Stunde";
            } else {
                return "§e" + hours + " §7Stunden";
            }
        } else if (minutes != 0) {
            if (minutes == 1) {
                return "§e" + 1 + " §7Minute";
            } else {
                return "§e" + minutes + " §7Minuten";
            }
        } else if (seconds != 0) {
            if (seconds == 1) {
                return "§e" + 1 + " §7Sekunde";
            } else {
                return "§e" + seconds + " §7Sekunden";
            }
        }
        return "Verbinde...";
    }
    private String getFriendsRaw(String uuid) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Friends WHERE UUID= ?");
            assert ps != null;
            ps.setString(1, uuid);
            ResultSet rs = ps.executeQuery();
            rs.next();
            return rs.getString("FriendsList");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public List<String> getFriendList(String uuid) {
        String friends = getFriendsRaw(uuid);
        List<String> returner = new ArrayList<>();
        if (friends.isEmpty())
            return returner;
        String[] friendsreturn = friends.split(";");
        Collections.addAll(returner, friendsreturn);
        return returner;
    }

    public int getFriendsAsInt(String uuid) {
        String friends = getFriendsRaw(uuid);
        if (friends.isEmpty())
            return 0;
        String[] friendsreturn = friends.split(";");

        return friendsreturn.length;
    }

    private String getRequestsRaw(String uuid) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Friends WHERE UUID= ?");
            assert ps != null;
            ps.setString(1, uuid);
            ResultSet rs = ps.executeQuery();
            rs.next();
            return rs.getString("FriendRequests");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public List<String> getRequestsList(String uuid) {
        String friends = getRequestsRaw(uuid);
        List<String> returner = new ArrayList<>();
        if (friends.isEmpty())
            return returner;
        String[] friendsreturn = friends.split(";");
        Collections.addAll(returner, friendsreturn);
        return returner;
    }

    public int getRequestsAsInt(String uuid) {
        String friends = getRequestsRaw(uuid);
        if (friends.isEmpty())
            return 0;
        String[] friendsreturn = friends.split(";");

        return friendsreturn.length;
    }
    //

    public void addFriend(String uuid, String newfriend) {
        String friendslist = getFriendsRaw(uuid);
        friendslist = friendslist + newfriend + ";";
        PreparedStatement ps = this.api.getNetworkSQL().getStatement("UPDATE Friends SET FriendsList= ? WHERE UUID= ?");
        try {
            assert ps != null;
            ps.setString(1, friendslist);
            ps.setString(2, uuid);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void remFriend(String uuid, String friend) {
        String friendslist = getFriendsRaw(uuid);
        friendslist = friendslist.replace(friend + ";", "");
        PreparedStatement ps = this.api.getNetworkSQL().getStatement("UPDATE Friends SET FriendsList= ? WHERE UUID= ?");
        try {
            assert ps != null;
            ps.setString(1, friendslist);
            ps.setString(2, uuid);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addRequest(String uuid, String newrequest) {
        String requestlist = getRequestsRaw(uuid);
        requestlist = requestlist + newrequest + ";";

        PreparedStatement ps = this.api.getNetworkSQL().getStatement("UPDATE Friends SET FriendRequests= ? WHERE UUID= ?");
        try {
            assert ps != null;
            ps.setString(1, requestlist);
            ps.setString(2, uuid);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void remRequest(String uuid, String request) {
        String Requestslist = getRequestsRaw(uuid);
        Requestslist = Requestslist.replace(request + ";", "");

        PreparedStatement ps = this.api.getNetworkSQL().getStatement("UPDATE Friends SET FriendRequests= ? WHERE UUID= ?");
        try {
            assert ps != null;
            ps.setString(1, Requestslist);
            ps.setString(2, uuid);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    //

    public HashMap<String, List<String>> getListSplited(String uuid) {
        List<String> friends = getFriendList(uuid);

        List<String> offline = new ArrayList<>();
        List<String> online = new ArrayList<>();
        for (String friend : friends) { // FRIEND -> UUID
            boolean on = this.api.getUUIDFetcher().friendsData(friend,"CurrentOnline");
            String friendname = this.api.getUUIDFetcher().getNamebyUUID(friend);
            if (on) {
                online.add(friendname);
            } else {
                offline.add(friendname);
            }
        }

        Collections.sort(online);
        Collections.sort(offline);
        HashMap<String, List<String>> hash = new HashMap<>();
        hash.put("online", online);
        hash.put("offline", offline);

        return hash;
    }

    public Integer getOnlineFriendsAsInt(String uuid) {
        return getListSplited(uuid).get("online").size();
    }
    public void setStatus(Player p, String status){
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("UPDATE Friends SET Status=? WHERE UUID =?");
            ps.setString(1, String.valueOf(status));
            ps.setString(2,p.getUniqueId().toString());
            ps.executeUpdate();
            ps.close();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    public String getStatus(String uuid) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Friends WHERE UUID= ?");
            ps.setString(1, uuid);
            ResultSet rs = ps.executeQuery();
            rs.next();
            String last = rs.getString("Status");
            if(last.equals(""))last = null;
            rs.close();
            ps.close();
            return last;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
    public boolean hasStatus(String uuid){
        return getStatus(uuid) != null;
    }
}
