/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.api;

import net.beteax.APIManager;
import net.beteax.BeteaxAPI;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class UtilAPI {

    private BeteaxAPI main;
    private APIManager api;

    public UtilAPI(BeteaxAPI main, APIManager api){
        this.main = main;
        this.api = api;
    }

    public void sendActionBar(Player p, String message) {
        final String messageNeu = message.replace("_", " ");
        String s = ChatColor.translateAlternateColorCodes('&', messageNeu);
        IChatBaseComponent icbc = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + s + "\"}");
        PacketPlayOutChat bar = new PacketPlayOutChat(icbc, (byte) 2);
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(bar);
    }
}
