/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.api;


public class PrefixAPI {
    public PrefixAPI(){

    }
    public String bedwars = "§b§lBedWars §8× §7";
    public String quicksg = "§e§lQuickSG §8× §7";
    public String debugger = "§4§lDebugger §8× §7";
    public String partytime = "§a§lPartyTime §8× §7";
    public String knockout = "§b§lKnockOut §8× §7";
    public String beteax = "§9§lBeteax §8× §7";
    public String clan = "§b§lClan §8× §7";
    public String nick = "§5§lNick §8× §7";
    public String friend = "§e§lFriends §8× §7";
}
