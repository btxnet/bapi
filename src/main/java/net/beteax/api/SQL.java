/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.api;

import net.beteax.sql.MySQL;

import java.sql.*;

public class SQL extends MySQL {
    private String passwd;
    private String database;
    private Connection connection;

    public SQL(String passwd, String database) {
        this.passwd = passwd;
        this.database = database;
    }

    /** {@inheritDoc} */
    @Override
    public void connect() {
        if (isConnected()) return;
        try {
            this.connection = DriverManager.getConnection("jdbc:mysql://localhost:" + "3306" + "/" + this.database + "?autoReconnect=true", "root", this.passwd);
            System.out.println("MySQL -> Connection to Database: " + this.database);

        } catch (Exception ex) {
            System.err.println("MySQL -> Connection Initalize Failure: " + ex);
        }
    }

    /** {@inheritDoc} */
    @Override
    public void disconnect() {
        if (!isConnected()) return;
        try {
            this.connection.close();
        } catch (Exception ex) {
            System.err.println("MySQL -> Connection DC Failure: " + ex);
        }
    }

    /** {@inheritDoc} */
    @Override
    public boolean isConnected() {
        return this.connection != null;
    }

    /** {@inheritDoc} */
    @Override
    public PreparedStatement getStatement(String sql) {
        if (!isConnected()) return null;
        try {
            return this.connection.prepareStatement(sql);
        } catch (SQLException ex) {
            System.err.println("MySQL -> Statement Failure: " + ex);
        }
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public ResultSet getResult(String qry) {
        if (!isConnected()) return null;
        try {
            PreparedStatement ps = getStatement(qry);
            assert ps != null;
            return ps.executeQuery();
        } catch (Exception ex) {
            System.err.println("MySQL -> Result Failure: " + ex);
        }
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public void update(String qry) {
        if (!isConnected()) return;
        try {
            final PreparedStatement statement = this.connection.prepareStatement(qry);
            statement.executeUpdate();
            statement.close();
        } catch (SQLException ex) {
            System.err.println("MySQL -> UpdateQRY Failure: " + ex);
        }

    }
}
