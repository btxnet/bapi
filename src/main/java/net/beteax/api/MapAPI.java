/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.api;

import net.beteax.APIManager;
import net.beteax.BeteaxAPI;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class MapAPI {
    private BeteaxAPI main;
    private APIManager api;


    public MapAPI(BeteaxAPI main, APIManager api){
        this.main = main;
        this.api = api;
    }


    public void addMapFolder(String pluginname) {
        pluginname = pluginname+"_API";
        File o = new File("plugins/" + pluginname + "/Maps");
        if (!o.exists()) {
            o.mkdirs();
        }

    }

    public void addMapFile(String pluginname) {
        pluginname = pluginname+"_API";
        addMapFolder(pluginname);
        File f = new File("plugins/" + pluginname + "/Maps/maps.yml");
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void createDefaultMap(String pluginname, String mapname, String builders) {
        pluginname = pluginname+"_API";
        YamlConfiguration cfg = getFileCFG(pluginname);
        addMapFile(pluginname);
        List<String> list = cfg.getStringList("Maps");
        if (!list.contains(mapname)) {
            list.add(mapname);
            cfg.set("Maps", list);
        }
        if (getMapBuilder(mapname, pluginname) == null) {
            cfg.set("Builder." + mapname, builders);
        } else {
            if (getMapBuilder(mapname, pluginname).equals("N/A")) {
                cfg.set("Builder." + mapname, builders);
            }
        }
        try {
            cfg.save(getFile(pluginname));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public List<String> getmaps(String pluginname) {
        pluginname = pluginname+"_API";
        YamlConfiguration cfg = getFileCFG(pluginname);
        assert cfg != null;
        return cfg.getStringList("Maps");
    }

    public String getMapBuilder(String mapname, String pluginname) {
        pluginname = pluginname+"_API";
        String builders = "N/A";
        YamlConfiguration cfg = getFileCFG(pluginname);
        String t = cfg.getString("Builder." + mapname);
        if (t == null) {
            cfg.set("Builder." + mapname, "N/A");
            try {
                cfg.save(getFile(pluginname));
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Builder not found for Map: " + mapname);
        } else {
            builders = t;
        }
        return builders;
    }
    public File getFile(String pluginname) {
        pluginname = pluginname+"_API";
        File f = new File("plugins/" + pluginname + "/Maps/maps.yml");
        if (f.exists()) {
            return f;
        } else {
            System.out.println("[FilesystemAPI] Es gibt keine File für " + pluginname);
        }
        return null;
    }

    public YamlConfiguration getFileCFG(String pluginname) {
        pluginname = pluginname+"_API";
        File t = new File("plugins/" + pluginname + "/Maps/maps.yml");
        if (t.exists()) {
            return YamlConfiguration.loadConfiguration(t);
        } else {
            System.out.println("[FilesystemAPI] Es gibt keine Mapfile für " + pluginname);
        }
        return null;
    }
}
