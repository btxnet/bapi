/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.api;

import net.beteax.APIManager;
import net.beteax.resources.GenerateGameID;

import java.util.concurrent.ThreadLocalRandom;

public class GameID {
    private APIManager api;
    public GameID(APIManager api){
        this.api = api;
    }
    public String generateID(){
        return "#"+new GenerateGameID(9,ThreadLocalRandom.current()).nextString();
    }
}
