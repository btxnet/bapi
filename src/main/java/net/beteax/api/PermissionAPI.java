/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.api;

import com.avaje.ebean.validation.NotNull;
import net.beteax.APIManager;
import net.beteax.BeteaxAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

public class PermissionAPI {

    private BeteaxAPI main;
    private APIManager api;
    private String[] ranks = new String[]
            {
            "administrator",
            "developer",
            "srmoderator",
            "srbuilder",
            "moderator",
            "builder",
            "supporter",
            "testmoderator",
            "testsupporter",
            "premium",
            "youtuber",
            "premium+",
            "spieler",
            "testbuilder"
            };

    public PermissionAPI(BeteaxAPI main, APIManager api) {
        this.api = api;
        this.main = main;
    }

    public String[] getRanks() {
        return ranks;
    }

    public String getGroup(String uuid) {
        String out = "spieler";
        ResultSet rs = this.api.getNetworkSQL().getResult("SELECT * FROM UserGroupData WHERE UUID='" + uuid + "'");
        try {
            if (rs.next()) out = rs.getString("Groupname");
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return out;
    }

    public void setGroup(String groupname, String uuid) {
        String real = "spieler";
        for (String s : this.getRanks()) {
            if (s.equals(groupname)) {
                real = groupname;
                break;
            }
        }
        this.api.getNetworkSQL().update("UPDATE UserGroupData SET Groupname='" + real + "' WHERE UUID='" + uuid + "'");
    }

    public long getUntil(String uuid) {
        long out = -1;
        try {
            ResultSet rs = this.api.getNetworkSQL().getResult("SELECT * FROM UserGroupData WHERE UUID='" + uuid + "'");
            while (rs.next()) {
                out = convertToMillis(rs.getLong("Until"));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return out;
    }
    public void setUntil(long until, String uuid, boolean converttoseconds) {
        long real;
        if(converttoseconds) real = convertToSeconds(until);else real = until;
        this.api.getNetworkSQL().update("UPDATE UserGroupData SET Until='" + real + "' WHERE UUID='" + uuid + "'");
    }

    public void updatePlayer(Player p) {
        this.checkRank(p);
        String uuid = p.getUniqueId().toString();
        String r = getGroup(uuid);
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "pex user " + uuid + " group set " + r);
    }
    private Long getCurrentSeconds(){
        return convertToSeconds(System.currentTimeMillis());
    }
    private Long convertToMillis(long in){
        if(in == -1)return in;
        return TimeUnit.SECONDS.toMillis(in);
    }
    private Long convertToSeconds(long in){
        if(in == -1)return in;
        return TimeUnit.MILLISECONDS.toSeconds(in);
    }
    private void checkRank(Player p) {
        String uuid = p.getUniqueId().toString();
        long u = getUntil(uuid);
        if(u < getCurrentSeconds() && u != -1L){
            setGroup("spieler", uuid);
            setUntil(-1, uuid, false);
        }
    }

}
