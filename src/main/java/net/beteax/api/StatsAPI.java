/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.api;

import net.beteax.APIManager;
import net.beteax.BeteaxAPI;
import net.beteax.resources.Gamemodes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class StatsAPI {

    private String realgame;
    private Integer kills = 0;
    private Integer deaths = 0;
    private Integer wins = 0;
    private Integer loses = 0;
    private Integer beds = 0;
    private Integer tokens = 0;
    private Integer played = 0;
    private Double kd;
    private String uuid;
    private APIManager api;

    public StatsAPI(Gamemodes game) {
        if (game.equals(Gamemodes.QUICKSG)) {
            realgame = "QuickSG";
        } else if (game.equals(Gamemodes.BEDWARS)) {
            realgame = "BedWars";
        } else if (game.equals(Gamemodes.KNOCKOUT)) {
            realgame = "KnockOut";
        } else if (game.equals(Gamemodes.FLOORS)) {
            realgame = "Floors";
        } else if (game.equals(Gamemodes.PARTYTIME)) {
            realgame = "Partytime";
        }else {
            System.err.println("[API] StatsAPI > Enum Filer not found.");
        }
        api = BeteaxAPI.getAPI(); //Adding APIManager bc it's impossible for this Class to be converted into an API for Manager without complete recode.
    }

    public StatsAPI setPlayer(String uuid) {
        this.uuid = uuid;
        //this.api.getNetworkSQL().update("CREATE TABLE IF NOT EXISTS Stats (UUID VARCHAR(255), Game VARCHAR(255), Kills INT(255), Beds INT(255), Deaths INT(255), Wins INT(255), Loses INT(255),Played INT(255), Ingamecoins BIGINT)");
        this.api.getNetworkSQL().update("CREATE TABLE IF NOT EXISTS Stats (UUID VARCHAR(255), Game VARCHAR(255), Kills INT(255), Deaths INT(255), Wins INT(255), Loses INT(255),Beds INT(255),Tokens INT(255),Played INT(255), Ingamecoins BIGINT)");
        PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Stats WHERE Game=? AND UUID=?");
        try {
            ps.setString(1, realgame);
            ps.setString(2, uuid);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                this.kills = rs.getInt("Kills");
                this.deaths = rs.getInt("Deaths");
                this.wins = rs.getInt("Wins");
                this.loses = rs.getInt("Loses");
                this.beds = rs.getInt("Beds");
                this.played = rs.getInt("Played");
                this.tokens = rs.getInt("Tokens");

            } else {
              //  PreparedStatement reg = this.api.getNetworkSQL().getStatement("INSERT INTO Stats (UUID,Game,Kills,Beds,Deaths,Wins,Loses,Played,Ingamecoins) VALUES (?,?,?,?,?,?,?,?,?)");
                PreparedStatement reg = this.api.getNetworkSQL().getStatement("INSERT INTO Stats (UUID,Game,Kills,Deaths,Wins,Loses,Played,Ingamecoins,Beds,Tokens) VALUES (?,?,?,?,?,?,?,?,?,?)");
                reg.setString(1, uuid);
                reg.setString(2, realgame);
                reg.setInt(3, this.kills);
                reg.setInt(4, this.deaths);
                reg.setInt(5, this.wins);
                reg.setInt(6, this.loses);
                reg.setInt(7, this.played);
                reg.setInt(8, 0);
                reg.setInt(9, this.beds);
                reg.setInt(10, this.tokens);
                reg.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (kills == 0 && deaths == 0) {
            this.kd = 0.0;
        } else if (deaths == 0) {
            this.kd = (double) kills;
        } else {
            this.kd = (this.kills / this.deaths) * 100.0 / 100.0;
        }
        return this;
    }

    public StatsAPI addBed() {
        this.beds = this.beds+1;
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("UPDATE Stats SET Beds=? WHERE UUID=? AND Game=?");
            ps.setInt(1, this.beds);
            ps.setString(2, this.uuid);
            ps.setString(3, realgame);
            ps.executeUpdate();
        } catch (SQLException ignored) {
        }
        return this;
    }

    public StatsAPI addTokens(Integer anzahl) {
        this.tokens = this.tokens+anzahl;
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("UPDATE Stats SET Tokens=? WHERE UUID=? AND GAME=?");
            ps.setInt(1, this.tokens);
            ps.setString(2, this.uuid);
            ps.setString(3, realgame);
            ps.executeUpdate();
        } catch (SQLException ignored) {
        }
        return this;
    }

    public StatsAPI addKill() {
        this.kills = this.kills+1;
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("UPDATE Stats SET Kills=? WHERE UUID=? AND Game=?");
            ps.setInt(1, this.kills);
            ps.setString(2, this.uuid);
            ps.setString(3, realgame);
            ps.executeUpdate();
        } catch (SQLException ignored) {
            ignored.printStackTrace();
        }
        if (kills == 0 && deaths == 0) {
            this.kd = 0.0;
        } else if (deaths == 0) {
            this.kd = (double) kills;
        } else {
            this.kd = (this.kills / this.deaths) * 100.0 / 100.0;
        }
        return this;
    }
    public StatsAPI addGame() {
        this.played = this.played+1;
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("UPDATE Stats SET Played=? WHERE UUID=? AND Game=?");
            ps.setInt(1, this.played);
            ps.setString(2, this.uuid);
            ps.setString(3, realgame);
            ps.executeUpdate();
        } catch (SQLException ignored) {
            ignored.printStackTrace();
        }
        return this;
    }

    public StatsAPI addDeath() {
        this.deaths = this.deaths+1;
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("UPDATE Stats SET Deaths=? WHERE UUID=? AND Game=?");
            ps.setInt(1, this.deaths);
            ps.setString(2, this.uuid);
            ps.setString(3, realgame);
            ps.executeUpdate();
        } catch (SQLException ignored) {
            ignored.printStackTrace();
        }
        if (kills == 0 && deaths == 0) {
            this.kd = 0.0;
        } else if (deaths == 0) {
            this.kd = (double) kills;
        } else {
            this.kd = (this.kills / this.deaths) * 100.0 / 100.0;
        }
        return this;
    }

    public StatsAPI removeTokens(Integer anzahl) {
        this.tokens = this.tokens-anzahl;
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("UPDATE Stats SET Wins=? WHERE UUID=? AND Game=?");
            ps.setInt(1, this.tokens);
            ps.setString(2, this.uuid);
            ps.setString(3, realgame);
            ps.executeUpdate();
        } catch (SQLException ignored) {
        }
        return this;
    }

    public StatsAPI addWin() {
        this.wins = this.wins+1;
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("UPDATE Stats SET Wins=? WHERE UUID=? AND Game=?");
            ps.setInt(1, this.wins);
            ps.setString(2, this.uuid);
            ps.setString(3, realgame);
            ps.executeUpdate();
        } catch (SQLException ignored) {
        }
        return this;
    }

    public StatsAPI addLose() {
        this.loses = this.loses+1;
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("UPDATE Stats SET Loses=? WHERE UUID=? AND Game=?");
            ps.setInt(1, this.loses);
            ps.setString(2, this.uuid);
            ps.setString(3, realgame);
            ps.executeUpdate();
        } catch (SQLException ignored) {
        }
        return this;
    }

    public Integer getBeds() {
        return beds;
    }

    public Integer getKills() {
        return this.kills;
    }

    public Integer getDeaths() {
        return this.deaths;
    }

    public Integer getWins() {
        return this.wins;
    }

    public Integer getLoses() {
        return this.loses;
    }
    public Integer getGames() {
        return this.played;
    }

    public double getKD() {
        return this.kd;
    }

    public Integer getTokens() {
        return tokens;
    }
}
