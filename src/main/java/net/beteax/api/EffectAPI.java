/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.api;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

public class EffectAPI {

    private static  Constructor<?> packetConstructor = null;
    private static  Field[] fields = null;
    private static  boolean netty = true;
    private static Field player_connection = null;
    private static Method player_sendPacket = null;
    private static HashMap<Class<? extends Entity>, Method> handles = new HashMap<>();

    private static boolean newParticlePacketConstructor = false;
    private static Class<Enum> enumParticle = null;
    private static  boolean compatible = true;

    static {
        String vString = getVersion().replace("v", "");
        double v = 0;
        if (!vString.isEmpty()) {
            String[] array = vString.split("_");
            v = Double.parseDouble(array[0] + "." + array[1]);
        }
        try {
            Class<?> packetClass;
            if (v < 1.7) {
                netty = false;
                packetClass = getNmsClass("Packet63WorldParticles");
                packetConstructor = packetClass.getConstructor();
                fields = packetClass.getDeclaredFields();
            } else {
                packetClass = getNmsClass("PacketPlayOutWorldParticles");
                if (v < 1.8) {
                    packetConstructor = packetClass.getConstructor(String.class, float.class, float.class, float.class,
                            float.class, float.class, float.class, float.class, int.class);
                } else { // use the new constructor for 1.8
                    newParticlePacketConstructor = true;
                    enumParticle = (Class<Enum>) getNmsClass("EnumParticle");
                    packetConstructor = packetClass.getDeclaredConstructor(enumParticle, boolean.class, float.class,
                            float.class, float.class, float.class, float.class, float.class, float.class, int.class,
                            int[].class);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            Bukkit.getLogger().severe("[ParticleLib] Failed to initialize NMS components!");
            compatible = false;
        }
    }

    private ParticleType type;
    private double speed;
    private int count;
    private double radius;


    public EffectAPI(ParticleType type, double speed, int count, double radius) {
        this.type = type;
        this.speed = speed;
        this.count = count;
        this.radius = radius;
    }

    private static void sendPacket(Player p, Object packet) throws IllegalArgumentException {
        try {
            if (player_connection == null) {
                player_connection = getHandle(p).getClass().getField("playerConnection");
                for (Method m : player_connection.get(getHandle(p)).getClass().getMethods()) {
                    if (m.getName().equalsIgnoreCase("sendPacket")) {
                        player_sendPacket = m;
                    }
                }
            }
            player_sendPacket.invoke(player_connection.get(getHandle(p)), packet);
        } catch (IllegalAccessException | NoSuchFieldException | InvocationTargetException ex) {
            ex.printStackTrace();
            Bukkit.getLogger().severe("[ParticleLib] Failed to send packet!");
        }
    }

    private static Object getHandle(Entity entity) {
        try {
            if (handles.get(entity.getClass()) != null)
                return handles.get(entity.getClass()).invoke(entity);
            else {
                Method entity_getHandle = entity.getClass().getMethod("getHandle");
                handles.put(entity.getClass(), entity_getHandle);
                return entity_getHandle.invoke(entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private static Class<?> getNmsClass(String name) {
        String version = getVersion();
        String className = "net.minecraft.server." + version + name;
        Class<?> clazz = null;
        try {
            clazz = Class.forName(className);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            Bukkit.getLogger().severe("[ParticleLib] Failed to load NMS class " + name + "!");
        }
        return clazz;
    }

    private static String getVersion() {
        String[] array = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",");
        if (array.length == 4)
            return array[3] + ".";
        return "";
    }

    public static boolean isCompatible() {
        return compatible;
    }

    public double getSpeed() {
        return speed;
    }

    public int getCount() {
        return count;
    }

    public double getRadius() {
        return radius;
    }

    public void sendToLocation(Location location) {
        try {
            Object packet = createPacket(location);
            for (Player player : Bukkit.getOnlinePlayers()) {
                sendPacket(player, packet);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Object createPacket(Location location) {
        try {
            if (this.count <= 0) {
                this.count = 1;
            }
            Object packet;
            if (netty) {
                if (newParticlePacketConstructor) {
                    Object particleType = enumParticle.getEnumConstants()[type.getId()];
                    packet = packetConstructor.newInstance(particleType,
                            true, (float) location.getX(), (float) location.getY(), (float) location.getZ(),
                            (float) this.radius, (float) this.radius, (float) this.radius,
                            (float) this.speed, this.count, new int[0]);
                } else {
                    packet = packetConstructor.newInstance(type.getName(),
                            (float) location.getX(), (float) location.getY(), (float) location.getZ(),
                            (float) this.radius, (float) this.radius, (float) this.radius,
                            (float) this.speed, this.count);
                }
            } else {
                packet = packetConstructor.newInstance();
                for (Field f : fields) {
                    f.setAccessible(true);
                    switch (f.getName()) {
                        case "a":
                            f.set(packet, type.getName());
                            break;
                        case "b":
                            f.set(packet, (float) location.getX());
                            break;
                        case "c":
                            f.set(packet, (float) location.getY());
                            break;
                        case "d":
                            f.set(packet, (float) location.getZ());
                            break;
                        case "e":
                        case "f":
                        case "g":
                            f.set(packet, this.radius);
                            break;
                        case "h":
                            f.set(packet, this.speed);
                            break;
                        case "i":
                            f.set(packet, this.count);
                            break;
                    }
                }
            }
            return packet;
        } catch (IllegalAccessException | InvocationTargetException | InstantiationException ex) {
            ex.printStackTrace();
            Bukkit.getLogger().severe("{ParticleLib] Failed to construct particle effect packet!");
        }
        return null;
    }

    public enum ParticleType {

        EXPLOSION_NORMAL("explode", 0, 17),
        EXPLOSION_LARGE("largeexplode", 1, 1),
        EXPLOSION_HUGE("hugeexplosion", 2, 0),
        FIREWORKS_SPARK("fireworksSpark", 3, 2),
        WATER_BUBBLE("bubble", 4, 3),
        WATER_SPLASH("splash", 5, 21),
        WATER_WAKE("wake", 6, -1),
        SUSPENDED("suspended", 7, 4),
        SUSPENDED_DEPTH("depthsuspend", 8, 5),
        CRIT("crit", 9, 7),
        CRIT_MAGIC("magicCrit", 10, 8),
        SMOKE_NORMAL("smoke", 11, -1),
        SMOKE_LARGE("largesmoke", 12, 22),
        SPELL("spell", 13, 11),
        SPELL_INSTANT("instantSpell", 14, 12),
        SPELL_MOB("mobSpell", 15, 9),
        SPELL_MOB_AMBIENT("mobSpellAmbient", 16, 10),
        SPELL_WITCH("witchMagic", 17, 13),
        DRIP_WATER("dripWater", 18, 27),
        DRIP_LAVA("dripLava", 19, 28),
        VILLAGER_ANGRY("angryVillager", 20, 31),
        VILLAGER_HAPPY("happyVillager", 21, 32),
        TOWN_AURA("townaura", 22, 6),
        NOTE("note", 23, 24),
        PORTAL("portal", 24, 15),
        ENCHANTMENT_TABLE("enchantmenttable", 25, 16),
        FLAME("flame", 26, 18),
        LAVA("lava", 27, 19),
        FOOTSTEP("footstep", 28, 20),
        CLOUD("cloud", 29, 23),
        REDSTONE("reddust", 30, 24),
        SNOWBALL("snowballpoof", 31, 25),
        SNOW_SHOVEL("snowshovel", 32, 28),
        SLIME("slime", 33, 29),
        HEART("heart", 34, 30),
        BARRIER("barrier", 35, -1),
        ITEM_CRACK("iconcrack_", 36, 33),
        BLOCK_CRACK("tilecrack_", 37, 34),
        BLOCK_DUST("blockdust_", 38, -1),
        WATER_DROP("droplet", 39, -1),
        ITEM_TAKE("take", 40, -1),
        MOB_APPEARANCE("mobappearance", 41, -1);

        private final String name;
        private final int id;
        private final int legacyId;

        ParticleType(String name, int id, int legacyId) {
            this.name = name;
            this.id = id;
            this.legacyId = legacyId;
        }

        String getName() {
            return name;
        }

        int getId() {
            return id;
        }

        int getLegacyId() {
            return legacyId;
        }
    }


}
