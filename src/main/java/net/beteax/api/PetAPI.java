/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.api;

import net.beteax.APIManager;
import net.beteax.BeteaxAPI;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PetAPI {
    private HashMap<Player, Entity> pets;
    private HashMap<Player, BukkitRunnable> petrunner;
    private BeteaxAPI main;
    private APIManager api;
    public PetAPI(BeteaxAPI main, APIManager api){
        this.api = api;
        this.main = main;
    }
    public List<Player> getActivePetUsers(){
        return new ArrayList<>(pets.keySet());
    }
    public List<Entity> getActivePets(){
        return new ArrayList<>(pets.values());
    }
    private boolean entityBlacklisted(EntityType et){
        return et.equals(EntityType.ARMOR_STAND) || et.equals(EntityType.ENDER_DRAGON) || et.equals(EntityType.WITHER);
    }

    /**
     *
     * @param p Player for the Enitiy
     * @param type Pet Enitiy
     * @return if it spawned sucessfully.
     */
    public boolean createPet(Player p, EntityType type){
        if(hasPet(p)) removePet(p);
        if(entityBlacklisted(type))return false;
        World world = p.getWorld();
        Entity e = world.spawnEntity(p.getLocation(), type);
        e.setCustomName("§7"+p.getName()+"'s Haustier");
        e.setCustomNameVisible(true);
        pets.put(p,e);
        petrunner.put(p, new BukkitRunnable() {
            @Override
            public void run() {
                Entity e = pets.get(p);
                if(e.getPassenger() != null){

                }
            }
        });
        petrunner.get(p).runTaskTimer(this.main, 20L, 20L);
        //TODO Scehdule
        return true;
    }
    public Location getPetLocation(Player p){
        return pets.get(p).getLocation();
    }
    public void removePet(Player p){
        if(hasPet(p)){
            pets.remove(p).remove();
            petrunner.remove(p).cancel();
        }
    }
    public boolean hasPet(Player p){
        return pets.containsKey(p);
    }
}
