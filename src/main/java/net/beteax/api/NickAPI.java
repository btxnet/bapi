/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.api;

import net.beteax.APIManager;
import net.beteax.BeteaxAPI;
import net.beteax.nick.Nick;
import net.beteax.nick.NickManager;
import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class NickAPI {
    private HashMap<String, String> nicked = new HashMap<>();
    private BeteaxAPI main;
    private APIManager api;

    public NickAPI(BeteaxAPI main, APIManager api) {
        this.main = main;
        this.api = api;
    }

    public boolean hasAutonick(Player p) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM UserNickData WHERE UUID=?");
            ps.setString(1, p.getUniqueId().toString());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int r = rs.getInt("AutoNick");
                return r != 0;
            }
        } catch (SQLException e) {

        }
        return false;
    }

    public boolean setAutonick(Player p, boolean value) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("UPDATE UserNickData SET AutoNick=? WHERE UUID=?");
            if (value) {
                ps.setInt(1, 1);
            } else {
                ps.setInt(1, 0);
            }
            ps.setString(2, p.getUniqueId().toString());
            ps.executeUpdate();
        } catch (SQLException e) {

        }
        return false;
    }

    public boolean hasPremiumPrefix(Player p) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM UserNickData WHERE UUID=?");
            ps.setString(1, p.getUniqueId().toString());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                String r = rs.getString("Prefix");
                return !r.equalsIgnoreCase("spieler");
            }
        } catch (SQLException ignored) {
        }
        return false;
    }

    public boolean setPrefix(Player p, String value) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("UPDATE UserNickData SET Prefix=? WHERE UUID=?");
            ps.setString(1, value);
            ps.setString(2, p.getUniqueId().toString());
            ps.executeUpdate();
        } catch (SQLException e) {

        }
        return false;
    }

    public void nick(Player p) {
        String randomname = NickManager.getnickname();
        this.nicked.put(p.getUniqueId().toString(), p.getName());
        NickManager.nicklist.put(p.getUniqueId().toString(), randomname);
        Nick.changePlayerName(p, randomname, false);
    }

    public void unnick(Player p) {
        Nick.changePlayerName(p, getRealname(p), true);
        NickManager.nicklist.remove(p.getUniqueId().toString());

    }

    public static boolean isNicked(Player p) {
        return NickManager.nicklist.containsKey(p.getUniqueId().toString());
    }

    public String getRealname(Player p) {
        return this.nicked.get(p.getUniqueId().toString());
    }
}
