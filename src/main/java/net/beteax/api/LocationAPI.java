/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.api;

import net.beteax.APIManager;
import net.beteax.BeteaxAPI;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class LocationAPI {
    private BeteaxAPI main;
    private APIManager api;

    public LocationAPI(BeteaxAPI main, APIManager api){
        this.api = api;
        this.main = main;
    }

    private void addLocationFolder(String pluginname) {
        pluginname = pluginname+"_API";
        File o = new File("plugins/" + pluginname + "/");
        if (!o.exists()) {
            o.mkdirs();
        }

    }

    private void addLocationFile(String pluginname) {
        pluginname = pluginname+"_API";
        addLocationFolder(pluginname);
        File f = new File("plugins/" + pluginname + "/locations.yml");
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //BEDWARS API START
    public File getFileSpawner(String pluginname, String type) {
        pluginname = pluginname+"_API";
        File f = new File("plugins/" + pluginname + "/Spawner/" + type + ".yml");
        if (f.exists()) {
            return f;
        } else {
            System.out.println("[FilesystemAPI] Es gibt keine File für " + pluginname);
        }
        return null;
    }

    private void addSpawnerFolder(String pluginname) {
        pluginname = pluginname+"_API";
        File o = new File("plugins/" + pluginname + "/Spawner/");
        if (!o.exists()) {
            o.mkdirs();
        }

    }

    private void addSpawnerFile(String pluginname, String type) {
        pluginname = pluginname+"_API";
        addLocationFolder(pluginname);
        File f = new File("plugins/" + pluginname + "/Spawner/" + type + ".yml");
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public YamlConfiguration getFileCFGSpawner(String pluginname, String type) {
        pluginname = pluginname+"_API";
        File f = new File("plugins/" + pluginname + "/Spawner/" + type + ".yml");
        if (f.exists()) {
            return YamlConfiguration.loadConfiguration(f);
        } else {
            System.out.println("[FilesystemAPI] Es gibt keine LocationFile für " + pluginname);
        }
        return null;
    }

    public void addSpawnerFiles(String pluginname, String type) {
        pluginname = pluginname+"_API";
        addSpawnerFolder(pluginname);
        addSpawnerFile(pluginname, type);
        File f = new File("plugins/" + pluginname + "/Spawner/" + type + ".yml");
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void setSpawner(String pluginname, String type, Location loc, String mapname) {
        pluginname = pluginname+"_API";
        YamlConfiguration cfg = getFileCFGSpawner(pluginname, type);
        int i = cfg.getInt("Spawner." + mapname);
        cfg.set(mapname + "." + type.toLowerCase() + "." + i + ".x", loc.getX());
        cfg.set(mapname + "." + type.toLowerCase() + "." + i + ".y", loc.getY());
        cfg.set(mapname + "." + type.toLowerCase() + "." + i + ".z", loc.getZ());
        cfg.set(mapname + "." + type.toLowerCase() + "." + i + ".pitch", loc.getPitch());
        cfg.set(mapname + "." + type.toLowerCase() + "." + i + ".yaw", loc.getYaw());
        cfg.set(mapname + "." + type.toLowerCase() + "." + i + ".world", loc.getWorld().getName());
        i++;
        cfg.set("Spawner." + mapname, i);
        try {
            cfg.save(getFileSpawner(pluginname, type));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Integer getValue(String pluginname, String mapname, String type) {
        pluginname = pluginname+"_API";
        YamlConfiguration cfg = getFileCFGSpawner(pluginname, type);
        return cfg.getInt("Spawner." + mapname);
    }
    public Object get(String pluginname, String mapname, String proc) {
        YamlConfiguration cfg = this.api.getMapAPI().getFileCFG(pluginname);
        assert cfg != null;
        return cfg.get(proc+"."+mapname);
    }
    public void set(String pluginname, String mapname, String proc,Object value) {
        YamlConfiguration cfg = this.api.getMapAPI().getFileCFG(pluginname);
        assert cfg != null;
        cfg.set(proc+"."+mapname,value);
        try {
            cfg.save(this.api.getMapAPI().getFile(pluginname));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Location getSpawnerLocationbyInt(String pluginname, String mapname, String type, int i) {
        pluginname = pluginname+"_API";
        YamlConfiguration cfg = getFileCFGSpawner(pluginname, type);

        double x = cfg.getDouble(mapname + "." + type.toLowerCase() + "." + i + ".x");
        double y = cfg.getDouble(mapname + "." + type.toLowerCase() + "." + i + ".y");
        double z = cfg.getDouble(mapname + "." + type.toLowerCase() + "." + i + ".z");
        double yaw = cfg.getDouble(mapname + "." + type.toLowerCase() + "." + i + ".yaw");
        double pitch = cfg.getDouble(mapname + "." + type.toLowerCase() + "." + i + ".pitch");
        String world = cfg.getString(mapname + "." + type.toLowerCase() + "." + i + ".world");

        World w = Bukkit.getWorld(world);
        return new Location(w, x, y, z, (float) yaw, (float) pitch);

    }
    //BEDWARS API ENDE

    public File getFile(String pluginname) {
        pluginname = pluginname+"_API";
        File f = new File("plugins/" + pluginname + "/locations.yml");
        if (f.exists()) {
            return f;
        } else {
            System.out.println("[FilesystemAPI] Es gibt keine File für " + pluginname);
        }
        return null;
    }

    public YamlConfiguration getFileCFG(String pluginname) {
        pluginname = pluginname+"_API";
        File t = new File("plugins/" + pluginname + "/locations.yml");
        if (t.exists()) {
            return YamlConfiguration.loadConfiguration(t);
        } else {
            System.out.println("[FilesystemAPI] Es gibt keine LocationFile für " + pluginname);
        }
        return null;
    }

    public void setLocation(String pluginname, String locationame, Location loc) {
        pluginname = pluginname+"_API";
        addLocationFolder(pluginname);
        addLocationFile(pluginname);
        YamlConfiguration cfg = getFileCFG(pluginname);
        cfg.set("Location." + locationame + ".x", loc.getX());
        cfg.set("Location." + locationame + ".y", loc.getY());
        cfg.set("Location." + locationame + ".z", loc.getZ());
        cfg.set("Location." + locationame + ".pitch", loc.getPitch());
        cfg.set("Location." + locationame + ".yaw", loc.getYaw());
        cfg.set("Location." + locationame + ".world", loc.getWorld().getName());
        try {
            cfg.save(getFile(pluginname));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setMapLocationbyInt(String pluginname, String mapname, Location loc) {
        pluginname = pluginname+"_API";
        this.api.getMapAPI().addMapFolder(pluginname);
        this.api.getMapAPI().addMapFile(pluginname);
        YamlConfiguration cfg = this.api.getMapAPI().getFileCFG(pluginname);
        int i = cfg.getInt("Ints." + mapname);
        if (i == 0) i++;
        cfg.set("Location." + mapname + "." + i + ".x", loc.getX());
        cfg.set("Location." + mapname + "." + i + ".y", loc.getY());
        cfg.set("Location." + mapname + "." + i + ".z", loc.getZ());
        cfg.set("Location." + mapname + "." + i + ".pitch", loc.getPitch());
        cfg.set("Location." + mapname + "." + i + ".yaw", loc.getYaw());
        cfg.set("Location." + mapname + "." + i + ".world", loc.getWorld().getName());
        i++;
        cfg.set("Ints." + mapname, i);
        System.out.println(cfg.getInt("Ints." + mapname));
        try {
            cfg.save(this.api.getMapAPI().getFile(pluginname));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Integer getValue(String pluginname, String mapname) {
        pluginname = pluginname+"_API";
        return this.api.getMapAPI().getFileCFG(pluginname).getInt("Ints." + mapname);
    }

    public Location getMapLocationbyInt(String pluginname, String mapname, int i) {
        pluginname = pluginname+"_API";
        YamlConfiguration cfg = this.api.getMapAPI().getFileCFG(pluginname);

        double x = cfg.getDouble("Location." + mapname + "." + i + ".x");
        double y = cfg.getDouble("Location." + mapname + "." + i + ".y");
        double z = cfg.getDouble("Location." + mapname + "." + i + ".z");
        double yaw = cfg.getDouble("Location." + mapname + "." + i + ".yaw");
        double pitch = cfg.getDouble("Location." + mapname + "." + i + ".pitch");
        String world = cfg.getString("Location." + mapname + "." + i + ".world");

        World w = Bukkit.getWorld(world);
        return new Location(w, x, y, z, (float) yaw, (float) pitch);

    }

    public void setMapLocation(String pluginname, String mapname, Location loc, String locationname) {
        pluginname = pluginname+"_API";
        this.api.getMapAPI().addMapFolder(pluginname);
        this.api.getMapAPI().addMapFile(pluginname);
        YamlConfiguration cfg = this.api.getMapAPI().getFileCFG(pluginname);
        cfg.set("Location." + mapname + "." + locationname + ".x", loc.getX());
        cfg.set("Location." + mapname + "." + locationname + ".y", loc.getY());
        cfg.set("Location." + mapname + "." + locationname + ".z", loc.getZ());
        cfg.set("Location." + mapname + "." + locationname + ".pitch", loc.getPitch());
        cfg.set("Location." + mapname + "." + locationname + ".yaw", loc.getYaw());
        cfg.set("Location." + mapname + "." + locationname + ".world", loc.getWorld().getName());
        List<String> list = cfg.getStringList("Maps");
        if (!list.contains(mapname)) {
            list.add(mapname);
        }
        cfg.set("Maps", list);

        try {
            cfg.save(this.api.getMapAPI().getFile(pluginname));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Location getMapLocation(String pluginname, String mapname, String locationname) {
        pluginname = pluginname+"_API";
        YamlConfiguration cfg = this.api.getMapAPI().getFileCFG(pluginname);

        double x = cfg.getDouble("Location." + mapname + "." + locationname + ".x");
        double y = cfg.getDouble("Location." + mapname + "." + locationname + ".y");
        double z = cfg.getDouble("Location." + mapname + "." + locationname + ".z");
        double yaw = cfg.getDouble("Location." + mapname + "." + locationname + ".yaw");
        double pitch = cfg.getDouble("Location." + mapname + "." + locationname + ".pitch");
        String world = cfg.getString("Location." + mapname + "." + locationname + ".world");
        World w = Bukkit.getWorld(world);
        return new Location(w, x, y, z, (float) yaw, (float) pitch);

    }

    public Location getLocation(String pluginname, String locationame) {
        pluginname = pluginname+"_API";
        YamlConfiguration cfg = getFileCFG(pluginname);
        double x = cfg.getDouble("Location." + locationame + ".x");
        double y = cfg.getDouble("Location." + locationame + ".y");
        double z = cfg.getDouble("Location." + locationame + ".z");
        double yaw = cfg.getDouble("Location." + locationame + ".yaw");
        double pitch = cfg.getDouble("Location." + locationame + ".pitch");
        String world = cfg.getString("Location." + locationame + ".world");
        World w = Bukkit.getWorld(world);
        return new Location(w, x, y, z, (float) yaw, (float) pitch);
    }

    /**
     * @param p            Spieler
     * @param locationname Location zu der der Spieler teleportiert werden soll
     * @param pluginname   Pluginname Exakt wie bei erstellung der Locations.
     */
    public void teleport(Player p, String locationname, String pluginname) {
        pluginname = pluginname+"_API";
        Location l = getLocation(pluginname, locationname);
        p.teleport(l);
    }
}
