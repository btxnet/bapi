/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.api;

import net.beteax.APIManager;
import net.beteax.BeteaxAPI;
import net.beteax.resources.Colors;
import net.beteax.sql.MySQL;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings("WeakerAccess")
public class ClanAPI {
    private BeteaxAPI main;
    private APIManager api;
    int maxinv = 15;
    int maxmember = 15;
    public ClanAPI(BeteaxAPI main, APIManager api){
        this.main = main;
        this.api = api;
        if(!this.api.isInit())System.out.println("[API]Couldn't Init " + this.getClass().getName() + " without an InitAPI.");
    }
    public int getMaxMembers(){
        return this.maxmember;
    }
    public int getMaxInvites(){
        return this.maxinv;
    }

    public boolean isClanExisting(String clanname) {
        return isClannameInuse(clanname);

    }

    public boolean isClanExistsByTag(String clantag) {
        return isTagInuse(clantag);

    }

    public boolean isTagInuse(final String clantag) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Clans WHERE Clantag=?");
            Objects.requireNonNull(ps).setString(1, clantag);
            ResultSet rs = ps.executeQuery();
            return rs.next();
        } catch (SQLException ignored) {
        }
        return true;
    }

    public boolean isClanExistsByID(final String clanid) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Clans WHERE ClanID=?");
            Objects.requireNonNull(ps).setString(1, clanid);
            ResultSet rs = ps.executeQuery();
            return rs.next();
        } catch (SQLException ignored) {
        }
        return true;
    }

    public boolean isClannameInuse(final String clanname) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Clans WHERE Clanname=?");
            Objects.requireNonNull(ps).setString(1, clanname);
            ResultSet rs = ps.executeQuery();
            return rs.next();
        } catch (SQLException ignored) {
        }
        return true;
    }

    private String generateClanID(String clanname, String clantag) {
        Random r = new Random();
        int c = r.nextInt(9999);
        String end = String.valueOf(c) + clanname.toLowerCase().replace("n", "")
                + clantag.toLowerCase().replace("n", "");
        return "#" + end;
    }

    public void removeAllInvites(String uuid) {
        String list = getInvitesRaw(uuid);
        if (!(list.isEmpty()) && isPlayerInClan(uuid)) {
            list = "";
            try {
                PreparedStatement ps = this.api.getNetworkSQL().getStatement("UPDATE Clansystem SET Invites=? WHERE UUID=?");
                Objects.requireNonNull(ps).setString(1, list);
                ps.setString(2, uuid);
                ps.executeUpdate();
                ps.close();
            } catch (SQLException ignored) {
            }
        }
    }

    public boolean hasInvite(String clanname, String uuid) {
        try {
            String[] list = getInvitesAsStringlist(uuid);
            for (String query : Objects.requireNonNull(list)) {
                if (clanname.equals(query)) {
                    return true;
                }
            }
        } catch (Exception ignored) {
        }
        return false;
    }

    public void addInvite(String clanname, String uuid) {
        try {
            String list = getInvitesRaw(clanname);
            list = list + clanname + ";";
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("UPDATE Clansystem SET Invites=? WHERE UUID=?");
            Objects.requireNonNull(ps).setString(1, list);
            ps.setString(2, uuid);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ignored) {
        }
    }

    public void removeInvite(String clanname, String uuid) {
        try {
            String invitelist = getInvitesRaw(uuid);
            invitelist = invitelist.replace(clanname + ";", "");
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("UPDATE Clansystem SET Invites=? WHERE UUID=?");
            Objects.requireNonNull(ps).setString(1, invitelist);
            ps.setString(2, uuid);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ignored) {
        }
    }

    public boolean canbeInvited(String uuid) {
        return this.api.getUUIDFetcher().SettingEnabled(uuid,"ClanRequests");
    }

    public void addMember(String clanname, String uuidfrommember) {
        try {
            String memberlist = getMembersAsString(clanname);
            memberlist = memberlist + uuidfrommember + ";";
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("UPDATE Clans SET Member=? WHERE Clanname=?");
            PreparedStatement ps2 = this.api.getNetworkSQL().getStatement("UPDATE Clansystem SET Clanname=? WHERE UUID=?");
            Objects.requireNonNull(ps2).setString(1, clanname);
            ps2.setString(2, uuidfrommember);
            ps2.executeUpdate();
            Objects.requireNonNull(ps).setString(1, memberlist);
            ps.setString(2, clanname);
            ps.executeUpdate();
            ps.close();
            ps2.close();

        } catch (SQLException ignored) {
        }
    }

    public Integer getOnlineAsInt(String clanname) {
        int i = 0;
        String[] mod = getModsAsStringlist(clanname);
        String[] user = getMembersAsStringlist(clanname);
        Player l = Bukkit.getPlayer(getLeadername(clanname));
        if (l != null) {
            i++;
        }
        for (String m : Objects.requireNonNull(mod)) {
            Player p = Bukkit.getPlayer(this.api.getUUIDFetcher().getNamebyUUID(m));
            if (p != null) {
                i++;
            }
        }
        for (String u : Objects.requireNonNull(user)) {
            Player p = Bukkit.getPlayer(this.api.getUUIDFetcher().getNamebyUUID(u));
            if (p != null) {
                i++;
            }
        }
        return i;
    }

    public Integer getPlayersAsInt(String clanname) {
        return getModsAsInt(clanname) + getMembersAsInt(clanname) + 1;
    }

    private String getInvitesRaw(String uuid) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Clansystem WHERE UUID=?");
            Objects.requireNonNull(ps).setString(1, uuid);
            ResultSet rs = ps.executeQuery();
            rs.next();
            String list = rs.getString("Invites");
            rs.close();
            ps.close();
            return list;
        } catch (SQLException ignored) {
        }
        return "";
    }

    public String[] getInvitesAsStringlist(String uuid) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Clansystem WHERE UUID=?");
            Objects.requireNonNull(ps).setString(1, uuid);
            ResultSet rs = ps.executeQuery();
            rs.next();
            String list = rs.getString("Invites");
            String[] out = list.split(";");
            rs.close();
            ps.close();
            return out;
        } catch (SQLException ignored) {
        }
        return null;
    }

    public boolean isPlayerInClan(String uuid) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Clansystem WHERE UUID=?");
            Objects.requireNonNull(ps).setString(1, uuid);
            ResultSet rs = ps.executeQuery();
            rs.next();
            String clanname = rs.getString("Clanname");
            return !clanname.equals("NoClan");

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
    public String getTagColor(String clanname){
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Clans WHERE Clanname= ?");
            ps.setString(1, clanname);
            ResultSet rs = ps.executeQuery();
            rs.next();
            String a = rs.getString("TagColor");
            String colorcode = Colors.valueOf(a).getColorID();
            rs.close();
            ps.close();
            return colorcode;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "§a";
    }
    public void registerToShop(String clanid) {
        PreparedStatement ps = this.api.getNetworkSQL().getStatement("INSERT INTO clan_Clanshop (ClanID, Color) VALUES (?,'green;')");
        try {
            ps.setString(1, clanid);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public void removeFromShop(String clanid) {
        this.api.getNetworkSQL().update("DELETE FROM clan_Clanshop WHERE ClanID='" + clanid + "'");

    }
    public void addCoins(Integer value, String clanname) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("UPDATE Clans SET Clancoins=? WHERE Clanname=?");

            ps.setInt(1, getCoins(clanname)+value);
            ps.setString(2, clanname);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void removeCoins(Integer value, String clanname) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("UPDATE Clans SET Clancoins=? WHERE Clanname=?");

            ps.setInt(1, (getCoins(clanname)-value));
            ps.setString(2, clanname);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Integer getCoins(String clanname) {
        try {
            try {
                PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Clans WHERE Clanname=?");
                ps.setString(1, clanname);
                ResultSet rs = ps.executeQuery();
                rs.next();
                Integer tag = rs.getInt("Clancoins");
                rs.close();
                ps.close();
                return tag;
            } catch (SQLException ignored) {
            }
        } catch (Exception ignored) {
        }
        return null;
    }
    public Colors getTagColorAsEnum(String clanname){
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Clans WHERE Clanname= ?");
            ps.setString(1, clanname);
            ResultSet rs = ps.executeQuery();
            rs.next();
            String a = rs.getString("TagColor");
            Colors colorcode = Colors.valueOf(a);
            rs.close();
            ps.close();
            return colorcode;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
    public void setTagColor(Colors color, String clanname){
        PreparedStatement ps = this.api.getNetworkSQL().getStatement("UPDATE Clans SET TagColor= ? WHERE Clanname= ?");
        try {
            ps.setString(1,color.toString());
            ps.setString(2, clanname);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public Integer getNeededXP(Integer xp){
        int level = 1;
        if(xp < 2000){
            return 2000-xp;
        }
        if(xp < 3000){
            return 3000-xp;
        }
        if(xp < 5000){
            return 5000-xp;
        }
        if(xp < 8000){
            return 8000-xp;
        }
        if(xp < 10000){
            return 10000-xp;
        }
        if(xp < 12000){
            return 12000-xp;
        }
        if(xp < 14000){
            return 14000-xp;
        }
        if(xp < 16000){
            return 16000-xp;
        }
        if(xp < 18000){
            return 18000-xp;
        }
        if(xp < 22000){
            return 22000-xp;
        }
        if(xp < 25000){
            return 25000-xp;
        }
        if(xp < 27000){
            return 27000-xp;
        }
        if(xp < 30000){
            return 30000-xp;
        }
        if(xp < 32000){
            return 32000-xp;
        }
        if(xp < 35000){
            return 35000-xp;
        }
        if(xp < 37000){
            return 37000-xp;
        }
        if(xp < 39000){
            return 39000-xp;
        }
        if(xp < 42000){
            return 42000-xp;
        }
        if(xp < 45000){
            return 45000-xp;
        }
        if(xp < 47000){
            return 47000-xp;
        }
        if(xp < 51000){
            return 51000-xp;
        }
        if(xp < 53000){
            return 53000-xp;
        }
        if(xp < 55000){
            return 55000-xp;
        }
        if(xp < 58000){
            return 58000-xp;
        }
        if(xp < 61000){
            return 61000-xp;
        }
        if(xp < 63000){
            return 63000-xp;
        }
        if(xp < 66000){
            return 66000-xp;
        }
        if(xp < 69000){
            return 69000-xp;
        }
        if(xp < 72000){
            return 72000-xp;
        }
        if(xp < 76000){
            return 76000-xp;
        }
        return level;
    }
    public Integer getClanLevel(String clanname){
        int xp = getXP(clanname);
        int level = 1;
        int max = 10;
        if(xp < 2000){
            return 1;
        }
        if(xp < 3000){
            return 2;
        }
        if(xp < 5000){
            return 3;
        }
        if(xp < 8000){
            return 4;
        }
        if(xp < 10000){
            return 5;
        }
        if(xp < 12000){
            return 6;
        }
        if(xp < 14000){
            return 7;
        }
        if(xp < 16000){
            return 8;
        }
        if(xp < 18000){
            return 9;
        }
        if(xp < 22000){
            return 10;
        }
        if(xp < 25000){
            return 11;
        }
        if(xp < 27000){
            return 12;
        }
        if(xp < 30000){
            return 13;
        }
        if(xp < 32000){
            return 14;
        }
        if(xp < 35000){
            return 15;
        }
        if(xp < 37000){
            return 16;
        }
        if(xp < 39000){
            return 17;
        }
        if(xp < 42000){
            return 18;
        }
        if(xp < 45000){
            return 19;
        }
        if(xp < 47000){
            return 20;
        }
        if(xp < 51000){
            return 21;
        }
        if(xp < 53000){
            return 22;
        }
        if(xp < 55000){
            return 23;
        }
        if(xp < 58000){
            return 24;
        }
        if(xp < 61000){
            return 25;
        }
        if(xp < 63000){
            return 26;
        }
        if(xp < 66000){
            return 27;
        }
        if(xp < 69000){
            return 28;
        }
        if(xp < 72000){
            return 29;
        }
        if(xp < 76000){
            return 30;
        }
        return level;
    }

    public Integer getXP(String clanname){
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Clans WHERE Clanname= ?");
            ps.setString(1, clanname);
            ResultSet rs = ps.executeQuery();
            rs.next();
            int xp = rs.getInt("XP");
            rs.close();
            ps.close();
            return xp;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }
    public void addXP(Integer xp, String clanname){
        PreparedStatement ps = this.api.getNetworkSQL().getStatement("UPDATE Clans SET XP= ? WHERE Clanname= ?");
        try {
            int xps = getXP(clanname)+xp;
            assert ps != null;
            ps.setInt(1,xps);
            ps.setString(2, clanname);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeClanByID(String clanid) {
        this.api.getNetworkSQL().update("DELETE FROM Clans WHERE ClanID='" + clanid + "'");
    }

    public void removeClanbyClanname(String clanname) {
        String[] mods = getModsAsStringlist(clanname);
        String[] member = getMembersAsStringlist(clanname);
        for (String m : Objects.requireNonNull(mods)) {
            removeMod(clanname, m);
        }
        for (String u : Objects.requireNonNull(member)) {
            removeMember(clanname, u);

        }
        removeLeader(getLeaderUUID(clanname), clanname);
        this.api.getNetworkSQL().update("DELETE FROM Clans WHERE Clanname='" + clanname + "'");
    }

    private void removeLeader(String leaderuuid, String clanname) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("UPDATE Clansystem SET Clanname=? WHERE UUID=?");
            Objects.requireNonNull(ps).setString(1, "NoClan");
            ps.setString(2, leaderuuid);
            ps.executeUpdate();
        } catch (SQLException ignored) {
        }
    }

    private void removeMember(String clanname, String memberuuid) {
        try {
            String memberlist = getMembersAsString(clanname);
            memberlist = memberlist.replace(memberuuid + ";", "");
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("UPDATE Clans SET Member=? WHERE Clanname=?");
            PreparedStatement ps2 = this.api.getNetworkSQL().getStatement("UPDATE Clansystem SET Clanname=? WHERE UUID=?");
            Objects.requireNonNull(ps2).setString(1, "NoClan");
            ps2.setString(2, memberuuid);
            ps2.executeUpdate();
            Objects.requireNonNull(ps).setString(1, memberlist);
            ps.setString(2, clanname);
            ps.executeUpdate();
            ps.close();
            ps2.close();
        } catch (SQLException ignored) {
        }
    }

    private void removeMod(String clanname, String moduuid) {
        try {
            String modlist = getModsAsString(clanname);
            modlist = modlist != null ? modlist.replace(moduuid + ";", "") : null;
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("UPDATE Clans SET Mods=? WHERE Clanname=?");
            PreparedStatement ps2 = this.api.getNetworkSQL().getStatement("UPDATE Clansystem SET Clanname=? WHERE UUID=?");
            Objects.requireNonNull(ps2).setString(1, "NoClan");
            ps2.setString(2, moduuid);
            ps2.executeUpdate();
            Objects.requireNonNull(ps).setString(1, modlist);
            ps.setString(2, clanname);
            ps.executeUpdate();
            ps.close();
            ps2.close();
        } catch (SQLException ignored) {
        }
    }


    public void remove(String uuid, String clanname) {
        String uuidleader = getLeaderUUID(clanname);
        String[] uuidlistmod = getModsAsStringlist(clanname);
        String[] uuidlistmember = getMembersAsStringlist(clanname);
        if (isPlayerInClan(uuid)) {
            if (!Objects.equals(uuidleader, uuid)) {
                assert uuidlistmember != null;
                for (String blacklist : uuidlistmember) {
                    if (uuid.equalsIgnoreCase(blacklist)) {
                        removeMember(clanname, uuid);
                    }
                }
                for (String blacklist : Objects.requireNonNull(uuidlistmod)) {
                    if (uuid.equalsIgnoreCase(blacklist)) {
                        removeMod(clanname, uuid);
                    }
                }
            }
        }
    }

    public boolean isMod(String uuid, String clanname) {
        try {
            String[] modlist = getModsAsStringlist(clanname);
            for (String query : Objects.requireNonNull(modlist)) {
                if (uuid.equalsIgnoreCase(query)) {
                    return true;
                }
            }
        } catch (Exception ignored) {
        }
        return false;
    }

    public boolean isLeader(String uuid, String clanname) {
        try {
            String uuidleader = getLeaderUUID(clanname);
            return uuid.equals(uuidleader) || uuid.equals(uuidleader) || uuidleader.equals(uuid);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public String getPlayerClan(String uuid) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Clansystem WHERE UUID=?");
            Objects.requireNonNull(ps).setString(1, uuid);
            ResultSet rs = ps.executeQuery();
            rs.next();
            String clanname = rs.getString("Clanname");
            rs.close();
            ps.close();
            return clanname;

        } catch (SQLException ignored) {
        }
        return null;

    }

    public String getClantag(String clanname) {
        try {
            try {
                PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Clans WHERE Clanname=?");
                Objects.requireNonNull(ps).setString(1, clanname);
                ResultSet rs = ps.executeQuery();
                rs.next();
                String tag = rs.getString("Clantag");
                rs.close();
                ps.close();
                return tag;
            } catch (SQLException ignored) {
            }
        } catch (Exception ignored) {
        }
        return null;
    }

    public void promoteMember(String uuid, String clanname) {
        try {
            String mods = getModsAsString(clanname);
            String member = getMembersAsString(clanname);
            if (!isMod(uuid, clanname)) {
                member = member.replace(uuid + ";", "");
                mods = mods + uuid + ";";
                PreparedStatement ps = this.api.getNetworkSQL().getStatement("UPDATE Clans SET Member=? WHERE Clanname=?");
                Objects.requireNonNull(ps).setString(1, member);
                ps.setString(2, clanname);
                PreparedStatement ps2 = this.api.getNetworkSQL().getStatement("UPDATE Clans SET Mods=? WHERE Clanname=?");
                Objects.requireNonNull(ps2).setString(1, mods);
                ps2.setString(2, clanname);
                ps.executeUpdate();
                ps2.executeUpdate();
                ps.close();
            }
        } catch (SQLException ignored) {
        }
    }

    public void modifyClan(String query, String value, String clanid) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("UPDATE Clans SET " + query + "=? WHERE ClanID=?");

            Objects.requireNonNull(ps).setString(1, value);
            ps.setString(2, clanid);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public String getClanIDByClanname(String clanname) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Clans WHERE Clanname=?");
            Objects.requireNonNull(ps).setString(1, clanname);
            ResultSet rs = ps.executeQuery();
            rs.next();
            String id = rs.getString("ClanID");
            rs.close();
            ps.close();
            return id;
        } catch (SQLException ignored) {
        }
        return "Nicht vorhanden";

    }

    public String getClannameByID(String clanid) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Clans WHERE ClanID=?");
            Objects.requireNonNull(ps).setString(1, clanid);
            ResultSet rs = ps.executeQuery();
            rs.next();
            String id = rs.getString("Clanname");
            rs.close();
            ps.close();
            return id;
        } catch (SQLException ignored) {
        }
        return "Nicht vorhanden";

    }

    public String getClanIDByClantag(String clantag) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Clans WHERE Clantag=?");
            Objects.requireNonNull(ps).setString(1, clantag);
            ResultSet rs = ps.executeQuery();
            rs.next();
            String id = rs.getString("ClanID");
            rs.close();
            ps.close();
            return id;
        } catch (SQLException ignored) {
        }
        return "Nicht vorhanden";

    }

    public String getClanIDByLeaderUUID(String leaderuuid) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Clans WHERE Leader=?");
            Objects.requireNonNull(ps).setString(1, leaderuuid);
            ResultSet rs = ps.executeQuery();
            rs.next();
            String id = rs.getString("ClanID");
            rs.close();
            ps.close();
            return id;
        } catch (SQLException ignored) {
        }
        return "Nicht vorhanden";

    }

    public void demoteMod(String uuid, String clanname) {
        try {
            String mods = getModsAsString(clanname);
            String member = getMembersAsString(clanname);
            if (isMod(uuid, clanname)) {
                mods = Objects.requireNonNull(mods).replace(uuid + ";", "");
                member = member + uuid + ";";
                PreparedStatement ps = this.api.getNetworkSQL().getStatement("UPDATE Clans SET Member=? WHERE Clanname=?");
                Objects.requireNonNull(ps).setString(1, member);
                ps.setString(2, clanname);
                PreparedStatement ps2 = this.api.getNetworkSQL().getStatement("UPDATE Clans SET Mods=? WHERE Clanname=?");
                Objects.requireNonNull(ps2).setString(1, mods);
                ps2.setString(2, clanname);
                ps.executeUpdate();
                ps2.executeUpdate();
                ps.close();
            }
        } catch (SQLException ignored) {
        }
    }

    public String getLeaderUUID(String clanname) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Clans WHERE Clanname=?");
            Objects.requireNonNull(ps).setString(1, clanname);
            ResultSet rs = ps.executeQuery();
            rs.next();
            return rs.getString("Leader");
        }
        catch (SQLException ignored) {
        }
        return "Error";

    }
    public DyeColor getBaseColor(String clanname) {
        DyeColor out = null;
        String sqlqry = null;
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Clans WHERE Clanname=?");
            ps.setString(1, clanname);
            ResultSet rs = ps.executeQuery();
            rs.next();
            sqlqry = rs.getString("Patterns");
        } catch (SQLException e) {
            // TODO: handle exception
        }
        if(sqlqry == null)return DyeColor.WHITE;
        for (String listi : sqlqry.split(";"))
            out = listi.contains("BASE:") ? DyeColor.valueOf(listi.split(":")[1]) : DyeColor.WHITE;
        return out;
    }
    public HashMap<Integer,Pattern> getBannerPattern(String clanname){
        HashMap<Integer,Pattern> pat;
        ArrayList<String> t2;
        //String t = "BASE:WHITE;1:RED:HALF_HORIZONTAL;2:BLACK:RHOMBUS_MIDDLE;3:RED:STRIPE_TOP;4:WHITE:STRIPE_BOTTOM;5:BLACK:STRIPE_MIDDLE;6:WHITE:CIRCLE_MIDDLE;";
        String sqlqry = null;
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Clans WHERE Clanname=?");
            Objects.requireNonNull(ps).setString(1, clanname);
            ResultSet rs = ps.executeQuery();
            rs.next();
            sqlqry = rs.getString("Patterns");
        } catch (SQLException e) {
            // TODO: handle exception
        }
        t2 = Arrays.stream(sqlqry.split(";")).filter(listi -> !listi.contains("BASE:")).collect(Collectors.toCollection(ArrayList::new));
        pat = t2.stream().map(outs -> outs.split(":")).collect(Collectors.toMap(temp1 -> Integer.valueOf(temp1[0]), temp1 -> new Pattern(DyeColor.valueOf(temp1[1]), PatternType.valueOf(temp1[2])), (a, b) -> a, HashMap::new));
        return pat;
    }

    public String getLeadername(String clanname) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Clans WHERE Clanname=?");
            Objects.requireNonNull(ps).setString(1, clanname);
            ResultSet rs = ps.executeQuery();
            rs.next();
            String leaderuuid = rs.getString("Leader");
            return api.getUUIDFetcher().getNamebyUUID(leaderuuid);
        } catch (SQLException ignored) {
        }
        return "";

    }

    public int getMembersAsInt(String clanname) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Clans WHERE Clanname=?");
            Objects.requireNonNull(ps).setString(1, clanname);
            ResultSet rs = ps.executeQuery();
            int number;
            rs.next();
            String members = rs.getString("Member");
            String[] member = members.split(";");
            if (member[0].equals("")) {
                number = 0;
            } else {
                number = member.length;
            }
            return number;
        } catch (SQLException ignored) {
        }
        return 0;

    }

    public int getInvitesAsInt(String uuid) {
        int number;
        String[] invites = getInvitesAsStringlist(uuid);
        if (invites[0].equals("")) {
            number = 0;
        } else {
            number = invites.length;
        }
        return number;

    }

    private String getMembersAsString(String clanname) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Clans WHERE Clanname=?");
            Objects.requireNonNull(ps).setString(1, clanname);
            ResultSet rs = ps.executeQuery();
            rs.next();
            return rs.getString("Member");
        } catch (SQLException ignored) {
        }
        return "";

    }

    public String[] getMembersAsStringlist(String clanname) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Clans WHERE Clanname=?");
            Objects.requireNonNull(ps).setString(1, clanname);
            ResultSet rs = ps.executeQuery();
            rs.next();
            String members = rs.getString("Member");
            return members.split(";");
        } catch (SQLException ignored) {
        }
        return null;

    }

    public int getModsAsInt(String clanname) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Clans WHERE Clanname=?");
            Objects.requireNonNull(ps).setString(1, clanname);
            ResultSet rs = ps.executeQuery();
            rs.next();
            int zahl;
            String members = rs.getString("Mods");
            String[] member = members.split(";");
            if (member[0].equals("")) {
                zahl = 0;
            } else {
                zahl = member.length;
            }
            return zahl;
        } catch (SQLException ignored) {
        }
        return 0;

    }

    public String[] getModsAsStringlist(String clanname) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Clans WHERE Clanname=?");
            Objects.requireNonNull(ps).setString(1, clanname);
            ResultSet rs = ps.executeQuery();
            rs.next();
            String members = rs.getString("Mods");
            return members.split(";");
        } catch (SQLException ignored) {
        }
        return null;

    }

    private String getModsAsString(String clanname) {
        try {
            PreparedStatement ps = this.api.getNetworkSQL().getStatement("SELECT * FROM Clans WHERE Clanname=?");
            Objects.requireNonNull(ps).setString(1, clanname);
            ResultSet rs = ps.executeQuery();
            rs.next();
            return rs.getString("Mods");
        } catch (SQLException ignored) {
        }
        return null;

    }
}
