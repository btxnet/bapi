/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.api;

import net.beteax.APIManager;
import net.beteax.BeteaxAPI;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.channels.Channels;

public class ServerChannelAPI {
    private BeteaxAPI main;
    private APIManager api;

    public ServerChannelAPI(BeteaxAPI main, APIManager api){
        this.api = api;
        this.main = main;
    }
    public void sendMessage(String message, Player user){
        ByteArrayOutputStream s = new ByteArrayOutputStream();
        DataOutputStream o = new DataOutputStream(s);
        try {
            o.writeUTF(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.main.getServer().getPlayerExact(user.getName()).sendPluginMessage(this.main, "API", s.toByteArray());
    }
}
