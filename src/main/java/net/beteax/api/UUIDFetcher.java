/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.api;

import net.beteax.APIManager;
import net.beteax.BeteaxAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Objects;
/*
                  Beteax UUIDFetcher by Variiuz c:
 */

public class UUIDFetcher {
    private HashMap<String, String> namecache = new HashMap<>();
    private HashMap<String, String> uuidcache = new HashMap<>();
    private BeteaxAPI main;
    private APIManager api;


    public UUIDFetcher(BeteaxAPI main, APIManager api) {
        this.api = api;
        this.main = main;
    }

    /**
     * Returns the UUID String saved on our Databases
     *
     * @param name The Username
     * @return UUID
     */
    public String getUUIDbyName(String name) {
        return getUUID(name);
    }

    /**
     * Return the Name saved on our Database
     *
     * @param uuid UUID String from the needed User
     * @return String
     */
    public String getNamebyUUID(String uuid) {
        return getName(uuid);
    }

    /**
     * Returns the Rankcolor of the User with the input UUID
     *
     * @param uuid     UUID of the Player
     * @param clanname The Clanname of the Players Clan
     * @return Color
     */
    public String getClanRankColor(String uuid, String clanname) {
        String color = "§a";
        if (api.getClanAPI().isMod(uuid, clanname)) {
            color = "§c";
        }
        if (api.getClanAPI().isLeader(uuid, clanname)) {
            color = "§4";
        }
        return color;

    }

    public String formNamewithRank(String name) {
        if (!isInDatabase(name)) return "§a" + name;
        return getRankColor(getUUIDbyName(name)) + name;
    }

    /**
     * Don't know why we had this Method. Because FirstJoin isn't allowed to be updated.
     * I still added it c: xD
     *
     * @param p Player
     */
    public void setFirstLogin(Player p) {
        try {
            PreparedStatement ps = api.getNetworkSQL().getStatement("UPDATE UserData SET FirstJoin=? WHERE UUID=?");
            assert ps != null;
            ps.setLong(1, System.currentTimeMillis());
            ps.setString(2, p.getUniqueId().toString());
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public long getFirstjoin(Player p) {
        PreparedStatement sql = api.getNetworkSQL().getStatement("SELECT * FROM UserData WHERE UUID=?");
        try {
            sql.setString(1, p.getUniqueId().toString());
            ResultSet rs = sql.executeQuery();
            rs.next();
            return rs.getLong("FirstJoin");
        } catch (SQLException ignored) {

        }
        return 0;
    }

    public String getRankColor(String uuid) {
        try {
            PreparedStatement ps = api.getNetworkSQL().getStatement("SELECT * FROM UserGroupData WHERE UUID=?");
            assert ps != null;
            ps.setString(1, uuid);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) return rs.getString("RankColor");
        } catch (SQLException ignored) {
        }
        return "§a";
    }

    private String getName(String val) {
        if (!isInDatabaseUUID(val)) return null;
        String val2 = val.toLowerCase();//UID lower
        if (namecache.containsKey(val2)) return namecache.get(val2);
        try {
            ResultSet rs = this.api.getNetworkSQL().getResult("SELECT * FROM UserData WHERE UUID='" + val + "'");
            while (rs.next())
                namecache.put(val2, rs.getString("Username"));
            return namecache.get(val2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Clears all Caches made while Fetching
     */
    public void clearCache() {
        //Manual Method is public if you need it for smth
        namecache.clear();
        uuidcache.clear();
    }

    public void startCacheClearer() {
        Bukkit.getScheduler().runTaskTimerAsynchronously(this.main, this::clearCache, 400L, 400L);
    }

    private String getUUID(String val) {
        if (!isInDatabase(val)) return null;
        String val2 = val.toLowerCase();
        if (uuidcache.containsKey(val2)) return uuidcache.get(val2);
        try {
            ResultSet rs = this.api.getNetworkSQL().getResult("SELECT * FROM UserData WHERE Username='" + val + "'");
            rs.next();
            uuidcache.put(val2, rs.getString("UUID"));
            return uuidcache.get(val2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean findPlayerinUserData(String uuid) {
        try {
            PreparedStatement ps = api.getNetworkSQL().getStatement("SELECT * FROM UserData WHERE UUID=?");
            Objects.requireNonNull(ps).setString(1, uuid);
            ResultSet rs = ps.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean findPlayerinUserGroupData(String uuid) {
        try {
            PreparedStatement ps = api.getNetworkSQL().getStatement("SELECT * FROM UserGroupData WHERE UUID=?");
            Objects.requireNonNull(ps).setString(1, uuid);
            ResultSet rs = ps.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean findPlayerinUserSettings(String uuid) {
        try {
            PreparedStatement ps = api.getNetworkSQL().getStatement("SELECT * FROM UserSettings WHERE UUID=?");
            Objects.requireNonNull(ps).setString(1, uuid);
            ResultSet rs = ps.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean findPlayerinFriends(String uuid) {
        try {
            PreparedStatement ps = api.getNetworkSQL().getStatement("SELECT * FROM Friends WHERE UUID=?");
            Objects.requireNonNull(ps).setString(1, uuid);
            ResultSet rs = ps.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean findPlayerinClanData(String uuid) {
        try {
            PreparedStatement ps = api.getNetworkSQL().getStatement("SELECT * FROM Clansystem WHERE UUID=?");
            Objects.requireNonNull(ps).setString(1, uuid);
            ResultSet rs = ps.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean isInDatabase(String username) {
        try {
            PreparedStatement ps = api.getNetworkSQL().getStatement("SELECT * FROM UserData WHERE Username=?");
            Objects.requireNonNull(ps).setString(1, username);
            ResultSet rs = ps.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean isInDatabaseUUID(String uuid) {
        try {
            PreparedStatement ps = api.getNetworkSQL().getStatement("SELECT * FROM UserData WHERE UUID=?");
            Objects.requireNonNull(ps).setString(1, uuid);
            ResultSet rs = ps.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public long getOnlineTime(String uuid) {
        try {
            PreparedStatement sl = api.getNetworkSQL().getStatement("SELECT SUM(leaveTime - joinTime) AS time FROM UserOnlinetime WHERE UUID = '" + uuid + "' AND joinTime GROUP BY UUID");
            long onlineTime = 0;
            ResultSet rs = sl.executeQuery();
            while (rs.next()) {
                onlineTime += (rs.getLong("time"));
            }
            rs.close();

            return onlineTime;
        } catch (SQLException ignored) {

        }
        return 0;
    }

    public void addOnlineTime(String uuid, long joinTime, long leaveTime) {
        PreparedStatement sql = api.getNetworkSQL().getStatement("INSERT INTO UserOnlinetime(UUID, joinTime, leaveTime) VALUES ('" + uuid + "','" + joinTime + "','" + leaveTime + "')");
        try {
            sql.executeUpdate();
        } catch (SQLException ignored) {
            ignored.printStackTrace();
        }

    }

    public int getJoinTimes(Player p) {
        PreparedStatement sql = api.getNetworkSQL().getStatement("SELECT COUNT(*) AS total FROM UserOnlinetime WHERE UUID=?");
        try {
            sql.setString(1, p.getUniqueId().toString());
            ResultSet rs = sql.executeQuery();
            rs.next();
            return rs.getInt("total");
        } catch (SQLException ignored) {

        }
        return 0;
    }

    public int getJoinTimes(String uuid) {
        PreparedStatement sql = api.getNetworkSQL().getStatement("SELECT COUNT(*) AS total FROM UserOnlinetime WHERE UUID=?");
        try {
            sql.setString(1, uuid);
            ResultSet rs = sql.executeQuery();
            rs.next();
            return rs.getInt("total");
        } catch (SQLException ignored) {

        }
        return 0;
    }

    public boolean SettingEnabled(String uuid, String settingname) {
        try {
            PreparedStatement ps = api.getNetworkSQL().getStatement("SELECT * FROM UserSettings WHERE UUID=?");
            ps.setString(1, uuid);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) return rs.getInt(settingname) != 0;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
    public boolean friendsData(String uuid, String settingname) {
        try {
            PreparedStatement ps = api.getNetworkSQL().getStatement("SELECT * FROM Friends WHERE UUID=?");
            ps.setString(1, uuid);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) return rs.getInt(settingname) != 0;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
    public String getLastVersion(String uuid){
        try{
            ResultSet rs = api.getNetworkSQL().getResult("SELECT * FROM UserData WHERE UUID='"+uuid+"'");
            if(rs.next()){
                return rs.getString("LastVersion");
            }
            rs.close();
        }catch (Exception ignored){}
        return "";
    }
}
