/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.nick;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import net.beteax.BeteaxAPI;
import net.minecraft.server.v1_8_R3.Packet;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_8_R3.PacketPlayOutNamedEntitySpawn;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.lang.reflect.Field;
import java.util.Collection;

public class Nick {


    private static Field nameField = getField(GameProfile.class, "name");

    private static void removeFromTab(CraftPlayer cp) {
        PacketPlayOutPlayerInfo packet = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER,
                cp.getHandle());
        sendPacket(packet, cp);
    }

    private static void addToTab(CraftPlayer cp) {
        PacketPlayOutPlayerInfo packet = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, cp.getHandle());
        sendPacket(packet, cp);
    }


    private static void sendPacket(Packet<?> packet, Player p) {
        for (Player all : Bukkit.getOnlinePlayers()) {
            if (!all.hasPermission("nick.seereal")) {
                ((CraftPlayer) all).getHandle().playerConnection.sendPacket(packet);
            }
        }
    }

    private static Field getField(Class<?> clazz, String name) {

        try {
            Field field = clazz.getDeclaredField(name);
            field.setAccessible(true);
            return field;
        } catch (NoSuchFieldException | SecurityException e) {
            return null;
        }

    }

    //Experimental Method
    public static void changePlayerNameForPlayers(Player player, String name, boolean realskin) {
        CraftPlayer cp = (CraftPlayer) player;

        try {
            nameField.set(cp.getProfile(), name);
            if (realskin) {
                changePlayerSkin(cp, name);
            } else {
                changePlayerSkin(cp, NickManager.getRandomSkin());
            }
        } catch (IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }

        PacketPlayOutEntityDestroy destroyPlayer = new PacketPlayOutEntityDestroy(cp.getEntityId());
        sendPacket(destroyPlayer, player);

        removeFromTab(cp);


        new BukkitRunnable() {

            @Override
            public void run() {
                addToTab(cp);
                PacketPlayOutNamedEntitySpawn spawnPlayer = new PacketPlayOutNamedEntitySpawn(cp.getHandle());
                for (Player all : Bukkit.getOnlinePlayers()) {
                    if (!all.equals(player)) {
                        if (!all.hasPermission("nick.seereal")) {
                            ((CraftPlayer) all).getHandle().playerConnection.sendPacket(spawnPlayer);
                        }
                    }
                }
            }

        }.runTaskLater(BeteaxAPI.getInstance(), 4);
    }

    public static void changePlayerName(Player p, String name, boolean realskin) {
        CraftPlayer cp = (CraftPlayer) p;
        try {
            nameField.set(cp.getProfile(), name);
            if (realskin) changePlayerSkin(cp, name);
            else changePlayerSkin(cp, NickManager.getRandomSkin());
        } catch (IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }

        PacketPlayOutEntityDestroy destroyPlayer = new PacketPlayOutEntityDestroy(cp.getEntityId());
        sendPacket(destroyPlayer, p);

        removeFromTab(cp);


        new BukkitRunnable() {

            @Override
            public void run() {
                addToTab(cp);
                PacketPlayOutNamedEntitySpawn spawnPlayer = new PacketPlayOutNamedEntitySpawn(cp.getHandle());
                for (Player all : Bukkit.getOnlinePlayers())
                    if (!all.equals(p)) ((CraftPlayer) all).getHandle().playerConnection.sendPacket(spawnPlayer);
            }

        }.runTaskLater(BeteaxAPI.getInstance(), 4);


    }

    private static void changePlayerSkin(CraftPlayer cp, String skinname) {
        cp.getProfile();
        GameProfile skin = null;

        try {
            skin = GameProfileBuilder.fetch(UUIDFetcher.getUUID(skinname));
        } catch (Exception e) {
            NickManager.removeSkin(skinname);
            changePlayerSkin(cp, NickManager.getRandomSkin());

        }

        Collection<Property> props = skin.getProperties().get("textures");
        cp.getProfile().getProperties().removeAll("textures");
        cp.getProfile().getProperties().putAll("textures", props);

    }
}
