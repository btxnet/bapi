/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.nick;

import net.beteax.BeteaxAPI;
import net.beteax.sql.MySQL;
import org.bukkit.Bukkit;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Random;

public class NickManager {
    public static void initDB(){
        BeteaxAPI.getAPI().getNetworkSQL().update("CREATE TABLE IF NOT EXISTS Nick (Nicklist VARCHAR(20), Skins TEXT(20000), Nicks TEXT(20000))");
    }
    public static HashMap<String, String> nicklist = new HashMap<>();

    private static String getRawSkins() {
        try {
            PreparedStatement ps = BeteaxAPI.getAPI().getNetworkSQL().getStatement("SELECT * FROM Nick WHERE Nicklist='beteax'");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getString("Skins");
            }
        } catch (SQLException e) {

        }
        return null;
    }
    public static void removeSkin(String errskin) {
        String o = getRawSkins();
        o = o.replace(errskin + ";", "");
        try {
            PreparedStatement ps = BeteaxAPI.getAPI().getNetworkSQL().getStatement("UPDATE Nick SET Skins=? WHERE Nicklist='beteax'");
            ps.setString(1, o);
            ps.executeUpdate();
        } catch (SQLException e) {}
    }

    private static String[] SkinList() {
        String list = getRawSkins();
        return list.split(";");
    }

    public static String getRandomSkin() {
        String[] skins = SkinList();
        Random r = new Random();
        int i = r.nextInt(skins.length);
        return skins[i];
    }

    private static boolean validname(String name) {
        if (Bukkit.getOnlinePlayers().contains(name)) {
            validname(name);
            return false;
        }
        if (nicklist.containsValue(name)) {
            validname(name);
            return false;
        }
        return true;
    }

    private static String getRawnicks() {
        try {
            PreparedStatement ps = BeteaxAPI.getAPI().getNetworkSQL().getStatement("SELECT * FROM Nick WHERE Lists='beteax'");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getString("Nicks");
            }
        } catch (SQLException e) {

        }
        return null;
    }

    private static String[] NickList() {
        String list = getRawnicks();
        return list.split(";");
    }

    public static String getnickname() {
        String[] nicks = NickList();
        Random r = new Random();
        int i = r.nextInt(nicks.length);
        String result = nicks[i];
        if (validname(result)) {
            return result;
        }
        return null;
    }
}
