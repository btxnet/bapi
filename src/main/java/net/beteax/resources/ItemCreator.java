/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.resources;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.*;
import org.bukkit.potion.PotionEffectType;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.UUID;

public class ItemCreator {


    private ItemStack item;

    public ItemCreator(Material material, int amount) {
        this.item = new ItemStack(material, amount);
    }

    public ItemCreator(Material material, int amount, int data) {
        this.item = new ItemStack(material, amount, (short) data);
    }

    public ItemCreator(int id, int amount, int data) {
        this.item = new ItemStack(id, amount, (short) data);
    }

    public ItemCreator(int amount) {
        this.item = new ItemStack(Material.SKULL_ITEM, amount, (short) SkullType.PLAYER.ordinal());
    }

    public ItemCreator(ItemStack item) {
        this.item = item;
    }

    public ItemCreator setData(int data) {
        this.item.setDurability((short) data);
        return this;
    }

    public ItemCreator setMaterial(Material m) {
        this.item.setType(m);
        return this;
    }

    public ItemCreator setId(int id) {
        this.item.setTypeId(id);
        return this;
    }

    public ItemCreator setUnbreakable() {
        ItemMeta m = this.item.getItemMeta();
        m.spigot().setUnbreakable(true);

        return this;
    }

    public ItemCreator setAmount(int amount) {
        this.item.setAmount(amount);
        return this;
    }

    public ItemCreator setName(String name) {
        ItemMeta m = this.item.getItemMeta();
        m.setDisplayName(name);
        this.item.setItemMeta(m);
        return this;
    }

    public ItemCreator setLore(String... lore) {
        ItemMeta m = this.item.getItemMeta();
        m.setLore(Arrays.asList(lore));
        this.item.setItemMeta(m);
        return this;
    }

    public ItemCreator enchant(Enchantment ench, int lvl) {
        this.item.addUnsafeEnchantment(ench, lvl);
        return this;
    }

    public ItemCreator addFlags(ItemFlag flag) {
        ItemMeta m = this.item.getItemMeta();
        m.addItemFlags(flag);
        this.item.setItemMeta(m);
        return this;
    }

    public ItemCreator setLeatherColor(Color color) {
        LeatherArmorMeta m = (LeatherArmorMeta) this.item.getItemMeta();
        m.setColor(color);
        this.item.setItemMeta(m);
        return this;
    }

    public ItemCreator setSkullOwner(String owner) {
        SkullMeta m = (SkullMeta) this.item.getItemMeta();
        m.setOwner(owner);
        this.item.setItemMeta(m);
        return this;
    }

    public ItemCreator setPotionType(PotionEffectType type) {
        PotionMeta m = (PotionMeta) this.item.getItemMeta();
        m.setMainEffect(type);
        this.item.setItemMeta(m);
        return this;
    }

    public ItemCreator setBookAuthor(String author) {
        BookMeta m = (BookMeta) this.item.getItemMeta();
        m.setAuthor(author);
        this.item.setItemMeta(m);
        return this;
    }

    public ItemCreator setBookContent(String... pages) {
        BookMeta m = (BookMeta) this.item.getItemMeta();
        m.setPages(pages);
        this.item.setItemMeta(m);
        return this;
    }

    public ItemCreator setBookTitle(String title) {
        BookMeta m = (BookMeta) this.item.getItemMeta();
        m.setTitle(title);
        this.item.setItemMeta(m);
        return this;
    }

    public ItemCreator setBookMeta(String title, String author, String... pages) {
        BookMeta m = (BookMeta) this.item.getItemMeta();
        m.setTitle(title);
        m.setAuthor(author);
        m.setPages(pages);
        this.item.setItemMeta(m);
        return this;
    }

    public ItemCreator setEggType(EntityType type) {
        if ((this.item != null) && (this.item.getType() == Material.MONSTER_EGG) && (type != null) && (type.getName() != null)) {
            try {
                String version = org.bukkit.Bukkit.getServer().getClass().toString().split("\\.")[3];
                Class<?> craftItemStack = Class.forName("org.bukkit.craftbukkit." + version + ".inventory.CraftItemStack");

                Object nmsItemStack = craftItemStack.getDeclaredMethod("asNMSCopy", new Class[]{ItemStack.class}).invoke(null, this.item);
                Object nbtTagCompound = Class.forName("net.minecraft.server." + version + ".NBTTagCompound").newInstance();

                Field nbtTagCompoundField = nmsItemStack.getClass().getDeclaredField("tag");
                nbtTagCompoundField.setAccessible(true);

                nbtTagCompound.getClass().getMethod("setString", new Class[]{String.class, String.class}).invoke(nbtTagCompound, "id", type.getName());
                nbtTagCompound.getClass().getMethod("set", new Class[]{String.class, Class.forName("net.minecraft.server." + version + ".NBTBase")}).invoke(nbtTagCompoundField.get(nmsItemStack), "EntityTag", nbtTagCompound);

                this.item = ((ItemStack) craftItemStack.getDeclaredMethod("asCraftMirror", new Class[]{nmsItemStack.getClass()}).invoke(null, new Object[]{nmsItemStack}));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return this;
    }

    public ItemCreator setSkullTexture(String base64) {
        ItemMeta m = this.item.getItemMeta();
        GameProfile profile = new GameProfile(UUID.randomUUID(), null);
        profile.getProperties().put("textures", new Property("textures", base64));
        Field profileField = null;
        try {
            profileField = m.getClass().getDeclaredField("profile");
            profileField.setAccessible(true);
            profileField.set(m, profile);
        } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e1) {
            e1.printStackTrace();
        }
        this.item.setItemMeta(m);
        return this;
    }

    public ItemStack build() {
        return this.item;
    }
} 