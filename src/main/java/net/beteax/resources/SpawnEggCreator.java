/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.resources;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.SpawnEgg;

import java.util.Arrays;

public class SpawnEggCreator {

    private final ItemStack item;

    public SpawnEggCreator(String name, EntityType entityType) {
        this.item = new SpawnEgg(entityType).toItemStack(1);
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(name);
        this.item.setItemMeta(itemMeta);
    }

    public SpawnEggCreator(EntityType entityType) {
        this.item = new SpawnEgg(entityType).toItemStack(1);
        ItemMeta itemMeta = item.getItemMeta();
        this.item.setItemMeta(itemMeta);
    }

    public SpawnEggCreator setLore(String... lore) {
        ItemMeta m = this.item.getItemMeta();
        m.setLore(Arrays.asList(lore));
        this.item.setItemMeta(m);
        return this;
    }
    public SpawnEggCreator enchant(Enchantment ench, int lvl) {
        this.item.addUnsafeEnchantment(ench, lvl);
        return this;
    }

    public SpawnEggCreator addFlags(ItemFlag flag) {
        ItemMeta m = this.item.getItemMeta();
        m.addItemFlags(flag);
        this.item.setItemMeta(m);
        return this;
    }

    public ItemStack get() {
        return this.item;
    }
}
