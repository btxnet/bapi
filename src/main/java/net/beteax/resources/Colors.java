/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.resources;

public enum Colors {

    PINK("Pink","§d"),
    YELLOW("Gelb", "§e"),
    LIGHTBLUE("Hellblau","§9"),
    LILA("Lila","§5"),
    DARKBLUE("Dunkelblau","§1"),
    LIGHTRED("Hellrot", "§c"),
    GOLD("Gold","§6"),
    DARKRED("Dunkelrot","§4"),
    GREEN("Grün","§a"),
    CYAN("Türkis","§3"),
    DARKGREEN("Dunkelgrün","§2");

    private final String colorid;
    private final String colorname;

    Colors(final String colorname, final String colorid) {
        this.colorid = colorid;
        this.colorname = colorname;
    }

    /**
     * Getter for property 'colorID'.
     *
     * @return Value for property 'colorID'.
     */
    public String getColorID() {
        return this.colorid;
    }

    /**
     * Getter for property 'colorName'.
     *
     * @return Value for property 'colorName'.
     */
    public String getColorName() {
        return this.colorname;
    }

}
