/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.resources;


import com.avaje.ebeaninternal.server.lib.sql.Prefix;
import net.beteax.BeteaxAPI;
import net.beteax.api.PrefixAPI;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Syntaxes {
    public static PrefixAPI a = BeteaxAPI.getAPI().getPrefixes();
    public static void sendSyntax(Player p, String cmd) {
        p.sendMessage(a.beteax + "§7Bitte nutze: §e§l" + cmd + "");
    }

    public static void sendNoPlayer(CommandSender cs) {
        cs.sendMessage(a.beteax+"§cDu bist kein Spieler.");
    }
    public static void sendNoPermission(Player p, String cmd) {
        p.sendMessage(a.beteax + "§cDer Befehl §8[§e§l"+cmd+"§8] §cexistiert nicht.");
    }
}
