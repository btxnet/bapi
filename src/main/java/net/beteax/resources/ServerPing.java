/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.resources;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.*;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;

public class ServerPing {
    private InetSocketAddress address;
    private int timeout = 2000;
    private int pingVersion = -1;
    private int protocolVersion = -1;
    private String gameVersion;
    private String motd = "-1";
    private int port;
    private int playersOnline = -1;
    private int maxPlayers = -1;
    private boolean isonline;

    public ServerPing(String ip, int port) {
        InetSocketAddress inet = new InetSocketAddress(ip, port);
        this.setAddress(inet);
        this.fetchData();
        this.port = port;
    }

    public static void writeString(String s, DataOutput out) throws IOException {
        byte[] b = s.getBytes("UTF-8");
        writeVarInt(b.length, out);
        out.write(b);
    }

    public static String readString(DataInput in) throws IOException {
        int len = readVarInt(in);
        byte[] b = new byte[len];
        in.readFully(b);

        return new String(b, "UTF-8");
    }

    public static int readVarInt(DataInput input) throws IOException {
        int out = 0;
        int bytes = 0;
        byte in;
        do {
            in = input.readByte();

            out |= (in & 0x7F) << (bytes++ * 7);

            if (bytes > 32) {
                throw new RuntimeException("VarInt too big");
            }

        } while ((in & 0x80) == 0x80);

        return out;
    }

    public static void writeVarInt(int value, DataOutput output) throws IOException {
        int part;
        while (true) {
            part = value & 0x7F;

            value >>>= 7;
            if (value != 0) {
                part |= 0x80;
            }

            output.writeByte(part);

            if (value == 0) {
                break;
            }
        }
    }

    public boolean fetchData() {
        Socket socket = new Socket();
        DataOutputStream out = null;
        DataInputStream in = null;
        ByteArrayOutputStream frame = null;
        DataOutputStream frameOut = null;
        ByteArrayInputStream inPacket = null;
        DataInputStream inFrame = null;
        try {
            socket.setSoTimeout(this.timeout);
            socket.connect(this.address, getTimeout());
            out = new DataOutputStream(socket.getOutputStream());
            in = new DataInputStream(socket.getInputStream());
            frame = new ByteArrayOutputStream();
            frameOut = new DataOutputStream(frame);

            writeVarInt(0x00, frameOut);
            writeVarInt(4, frameOut);
            writeString(this.address.getHostString(), frameOut);
            frameOut.writeShort(this.address.getPort());
            writeVarInt(1, frameOut);
            writeVarInt(frame.size(), out);
            frame.writeTo(out);
            frame.reset();
            writeVarInt(0x00, frameOut);
            writeVarInt(frame.size(), out);
            frame.writeTo(out);
            frame.reset();

            int len = readVarInt(in);
            byte[] packet = new byte[len];
            in.readFully(packet);

            inPacket = new ByteArrayInputStream(packet);
            inFrame = new DataInputStream(inPacket);
            int id = readVarInt(inFrame);
            if (id != 0x00) {
                this.isonline = false;
            } else {
                JsonParser parser = new JsonParser();
                String json = readString(inFrame);
                JsonObject jsonObject = parser.parse(json).getAsJsonObject();
                JsonObject jsonPlayers = jsonObject.get("players").getAsJsonObject();
                this.playersOnline = Integer.parseInt(jsonPlayers.get("online").toString());
                this.maxPlayers = Integer.parseInt(jsonPlayers.get("max").toString());
                this.motd = jsonObject.get("description").toString().replaceAll("\"", "");
                this.isonline = true;
            }

        } catch (Exception exception) {
            if (!(exception instanceof ConnectException))
                this.isonline = false;
        } finally {
            try {
                if (inFrame != null)
                    inFrame.close();
            } catch (IOException exception) {
                exception.printStackTrace();
            }
            try {
                if (inPacket != null)
                    inPacket.close();
            } catch (IOException exception) {
                exception.printStackTrace();
            }
            try {
                if (frameOut != null)
                    frameOut.close();
            } catch (IOException exception) {
                exception.printStackTrace();
            }
            try {
                if (frame != null)
                    frame.close();
            } catch (IOException exception) {
                exception.printStackTrace();
            }
            try {
                if (in != null)
                    in.close();
            } catch (IOException exception) {
                exception.printStackTrace();
            }
            try {
                if (out != null)
                    out.close();
            } catch (IOException exception) {
                exception.printStackTrace();
            }
            try {
                socket.close();
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }

        return isonline;
    }

    public boolean isOnline() {
        return this.isonline;
    }

    public int getPort() {
        return this.port;
    }


    public InetSocketAddress getAddress() {
        return this.address;
    }

    public void setAddress(InetSocketAddress address) {
        this.address = address;
    }

    public int getTimeout() {
        return this.timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public int getPingVersion() {
        return this.pingVersion;
    }

    public int getProtocolVersion() {
        return this.protocolVersion;
    }

    public String getGameVersion() {
        return this.gameVersion;
    }

    public String getMotd() {
        return this.motd;
    }

    public int getPlayersOnline() {
        return this.playersOnline;
    }

    public int getMaxPlayers() {
        return this.maxPlayers;
    }


}
