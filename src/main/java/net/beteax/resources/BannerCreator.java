/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.resources;

import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.List;

public class BannerCreator {


    private ItemStack item;

    public BannerCreator() {
        this.item = new ItemStack(Material.BANNER,1);
    }
    public BannerCreator setUnbreakable() {
        ItemMeta m = this.item.getItemMeta();
        m.spigot().setUnbreakable(true);
        return this;
    }
    public BannerCreator setBannerColor(Color color){
        BannerMeta m = (BannerMeta) this.item.getItemMeta();
        m.setBaseColor(DyeColor.getByColor(color));
        this.item.setItemMeta(m);
        return this;
    }
    public BannerCreator setBannerPatterns(List<org.bukkit.block.banner.Pattern> bannerPatterns){
        BannerMeta m = (BannerMeta)this.item.getItemMeta();
        m.setPatterns(bannerPatterns);
        return this;
    }
    public BannerCreator setBannerPattern(Integer i,org.bukkit.block.banner.Pattern bannerPattern){
        BannerMeta m = (BannerMeta)this.item.getItemMeta();
        m.setPattern(i,bannerPattern);
        return this;
    }

    public BannerCreator setName(String name) {
        ItemMeta m = this.item.getItemMeta();
        m.setDisplayName(name);
        this.item.setItemMeta(m);
        return this;
    }

    public BannerCreator setLore(String... lore) {
        ItemMeta m = this.item.getItemMeta();
        m.setLore(Arrays.asList(lore));
        this.item.setItemMeta(m);
        return this;
    }

    public BannerCreator enchant(Enchantment ench, int lvl) {
        this.item.addUnsafeEnchantment(ench, lvl);
        return this;
    }

    public BannerCreator addFlags(ItemFlag flag) {
        ItemMeta m = this.item.getItemMeta();
        m.addItemFlags(flag);
        this.item.setItemMeta(m);
        return this;
    }


    public ItemStack build() {
        return this.item;
    }
} 