/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.resources;

public enum Gamemodes {

    QUICKSG, BEDWARS, KNOCKOUT, FLOORS, PARTYTIME;
}
