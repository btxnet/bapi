/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax;

import com.sun.org.glassfish.external.statistics.Stats;
import io.netty.channel.Channel;
import net.beteax.api.*;
import net.beteax.resources.Gamemodes;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class APIManager {

    private SQL databaseapi;
    private SQL permissions;
    private ClanAPI clanAPI;
    private UUIDFetcher uuidfetcher;
    private LocationAPI locapi;
    private MapAPI mapapi;
    private Coins coins;
    private boolean init = false;
    private BeteaxAPI main;
    private GameID gameidapi;
    private FriendsAPI fapi;
    private PermissionAPI permapi;
    private ServerChannelAPI chapi;
    private PetAPI petapi;
    private PrefixAPI prefixes;
    private HashMap<Player, StatsAPI> stats;
    private UtilAPI utils;
    private NickAPI nick;

    public APIManager(BeteaxAPI main){
        this.main = main;
    }
    
    /**
     * Do not use this a second time unless you know what to do!
     * @throws NullPointerException so that Error aren't so big
     * @param sqlpw
     * @param sqldb
     * @param permdb
     * @return APIManager Instance
     */
    APIManager initAPI(String sqlpw, String sqldb, String permdb) throws NullPointerException{
        databaseapi = new SQL(sqlpw, sqldb);
        databaseapi.connect();
        permissions = new SQL(sqlpw,permdb);
        permissions.connect();
        if(!(permissions.isConnected() || databaseapi.isConnected())){
            System.out.println("[API]Init stopped, SQL Data is wrong. Please restart with the right Values.");
            return null;
        }
        uuidfetcher = new UUIDFetcher(this.main, this);
        init = true;
        clanAPI = new ClanAPI(this.main, this);
        coins = new Coins(this.main, this);
        locapi = new LocationAPI(this.main, this);
        mapapi = new MapAPI(this.main, this);
        gameidapi = new GameID(this);
        stats = new HashMap<>();
        fapi = new FriendsAPI(this.main, this);
        permapi = new PermissionAPI(this.main, this);
        chapi = new ServerChannelAPI(this.main,this);
        prefixes = new PrefixAPI();
        utils = new UtilAPI(this.main,this);
        petapi = new PetAPI(this.main, this);
        nick = new NickAPI(this.main, this);
        return this;
    }
    public boolean isInit(){
        return init;
    }
    public SQL getNetworkSQL(){
        return databaseapi;
    }
    public SQL getPermissionsSQL(){
        return permissions;
    }
    public ClanAPI getClanAPI(){
        return clanAPI;
    }
    public UUIDFetcher getUUIDFetcher(){
        return uuidfetcher;
    }
    public Coins getCoinAPI(){
        return coins;
    }
    public GameID getGameID_API(){
        return gameidapi;
    }
    public LocationAPI getLocationAPI(){
        return locapi;
    }
    public StatsAPI initStatsUser(String uuid, Gamemodes gamemode, Player p){
        StatsAPI sta = new StatsAPI(gamemode).setPlayer(uuid);
        stats.put(p, sta);
        return stats.get(p);
    }
    public PrefixAPI getPrefixes(){
        return prefixes;
    }
    public PetAPI getPetAPI(){
        return petapi;
    }
    public UtilAPI getUtils(){
        return utils;
    }
    public PermissionAPI getPermissions(){
        return permapi;
    }
    public StatsAPI getStatsUser(Player p){
        return stats.get(p);
    }
    public MapAPI getMapAPI() {
        return mapapi;
    }
    public FriendsAPI getFriends(){
        return fapi;
    }
    public NickAPI getNickSystem(){return nick;}

    public ServerChannelAPI getChannelAPI() {
        return chapi;
    }
}
