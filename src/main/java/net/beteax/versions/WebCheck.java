/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.versions;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.beteax.BeteaxAPI;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Random;

public class WebCheck {
    private Random RDM = new Random();
    private Integer version = 1;
    private String revbuild = "Dev";
    /**
     * Getter for property 'currentRevnum'.
     *
     * @return Value for property 'currentRevnum'.
     */
    public Integer getCurrentRevnum(){
        return version;
    }
    public Integer getRevnum(String build){
        switch (build){
            case "Dev":{
                return getRevisionNumDev();
            }
            case "Live":{
                return getRevisionNumLive();
            }
            case "Newest":{
                return getNewestRevisionNum();
            }
            default:{
                return -1;
            }
        }
    }
    public boolean same(Integer newnum, Integer oldnum){
        return newnum.equals(oldnum) || oldnum.equals(newnum);
    }
    public String currentBuild(){return revbuild;}
    public static final String DEFAULT_URL = "https://patches.beteax.net/revision/"; //Default patch URL for Revision Checks

    /**
     * Getter for property 'newestVersion'.
     *
     * @return Value for property 'newestVersion'.
     */
    public String getNewestVersion() { return getString(DEFAULT_URL + "bapi/newest.php", "version");}
    /**
     * Getter for property 'newestRevision'.
     *
     * @return Value for property 'newestRevision'.
     */
    public String getNewestRevision() { return getString(DEFAULT_URL + "bapi/newest.php", "revision"); }
    /**
     * Getter for property 'newestRevisionNum'.
     *
     * @return Value for property 'newestRevisionNum'.
     */
    public Integer getNewestRevisionNum() { return getInt(DEFAULT_URL + "bapi/newest.php", "revnum"); }
    /**
     * Getter for property 'pluginName'.
     *
     * @return Value for property 'pluginName'.
     */
    public String getPluginName() { return getString(DEFAULT_URL + "bapi/newest.php", "name"); }
//
    public String getVersion(int revnum) {
        return getString(DEFAULT_URL + "bapi/"+revnum+".php", "version");
    }
    public String getRevision(int revnum) {
        return getString(DEFAULT_URL + "bapi/"+revnum+".php", "revnum");
    }
    public String getPluginName(int revnum) {
        return getString(DEFAULT_URL + "bapi/"+revnum+".php", "name");
    }
    /**
     * Getter for property 'versionDev'.
     *
     * @return Value for property 'versionDev'.
     */ //
    public String getVersionDev() {
        return getString(DEFAULT_URL + "bapi/dev.php", "version");
    }
    /**
     * Getter for property 'revisionDev'.
     *
     * @return Value for property 'revisionDev'.
     */
    public String getRevisionDev() {
        return getString(DEFAULT_URL + "bapi/dev.php", "revision");
    }
    /**
     * Getter for property 'revisionNumDev'.
     *
     * @return Value for property 'revisionNumDev'.
     */
    public Integer getRevisionNumDev() {
        return getInt(DEFAULT_URL + "bapi/dev.php", "revnum");
    }
    /**
     * Getter for property 'versionLive'.
     *
     * @return Value for property 'versionLive'.
     */ //
    public String getVersionLive() {
        return getString(DEFAULT_URL + "bapi/live.php", "version");
    }
    /**
     * Getter for property 'revisionLive'.
     *
     * @return Value for property 'revisionLive'.
     */
    public String getRevisionLive() {
        return getString(DEFAULT_URL + "bapi/live.php", "revision");
    }
    /**
     * Getter for property 'revisionNumLive'.
     *
     * @return Value for property 'revisionNumLive'.
     */
    public Integer getRevisionNumLive() {
        return getInt(DEFAULT_URL + "bapi/live.php", "revnum");
    }


    private <E> E handleRequest(String url, Type type) {
        try {
            URLConnection urlConnection = new java.net.URL(url).openConnection();
            urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
            urlConnection.setUseCaches(false);
            urlConnection.setConnectTimeout(1000);
            urlConnection.connect();

            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), StandardCharsets.UTF_8))) {
                JsonObject jsonObject = new JsonParser().parse(bufferedReader).getAsJsonObject();
                return BeteaxAPI.GSON.fromJson(jsonObject.get("result"), type);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getString(String url, String key) {
        try {
            URLConnection urlConnection = new java.net.URL(url).openConnection();
            urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
            urlConnection.setUseCaches(false);
            urlConnection.setConnectTimeout(1000);
            urlConnection.connect();

            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), StandardCharsets.UTF_8))) {
                JsonObject jsonObject = new JsonParser().parse(bufferedReader).getAsJsonObject();
                return jsonObject.get(key).getAsString();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    private Integer getInt(String url, String key) {
        try {
            URLConnection urlConnection = new java.net.URL(url).openConnection();
            urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
            urlConnection.setUseCaches(false);
            urlConnection.setConnectTimeout(1000);
            urlConnection.connect();

            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), StandardCharsets.UTF_8))) {
                JsonObject jsonObject = new JsonParser().parse(bufferedReader).getAsJsonObject();
                return jsonObject.get(key).getAsInt();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public void update(String version) {

    }


}
