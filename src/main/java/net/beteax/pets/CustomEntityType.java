/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.pets;

import net.minecraft.server.v1_8_R3.*;
import org.bukkit.entity.EntityType;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

public enum CustomEntityType {

    PIG("CoolPig", 90, EntityType.PIG, EntityPig.class, PigPet.class), WITHER("CoolBigiie", 64
            , EntityType.WITHER, EntityWither.class, Humanpet.class);

    private String name;
    private int id;
    private EntityType et;
    private Class<? extends EntityInsentient> nmsClass;
    private Class<? extends  EntityInsentient> customClass;

    private CustomEntityType(String name, int id, EntityType et, Class<? extends EntityInsentient> nmsClass, Class<? extends EntityInsentient> customClass) {
        this.name = name;
        this.id = id;
        this.et = et;
        this.nmsClass = nmsClass;
        this.customClass = customClass;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public EntityType getEt() {
        return et;
    }

    public Class<? extends EntityInsentient> getNmsClass() {
        return nmsClass;
    }

    public Class<? extends EntityInsentient> getCustomClass() {
        return customClass;
    }

    /**
     * Register entities.
     */
    public static void registerEntites() {
        for (CustomEntityType entity : values()) a(entity.getCustomClass(), entity.getName(), entity.getId());
        BiomeBase[] biomes;

        try {
            biomes = (BiomeBase[]) getPrivateStatic(BiomeBase.class, "biomes");
        } catch (Exception e) {
            //OOF FETCH
            return;
        }

        for (BiomeBase biomeBase : biomes) {
            if (biomeBase == null) {
                break;
            }

            for (String field : new String[] { "at", "au", "av", "aw"})
                try {
                Field list = BiomeBase.class.getDeclaredField(field);
                list.setAccessible(true);

                    List<BiomeBase.BiomeMeta> mobList = (List<BiomeBase.BiomeMeta>) list.get(biomeBase);

                    //Write our custom entity class.
                    for (BiomeBase.BiomeMeta meta : mobList) {
                        for (CustomEntityType type : values()) {
                            if (type.getNmsClass().equals(meta.b))
                                meta.b = type.getCustomClass();
                        }
                    }
                } catch (Exception e) {
                e.printStackTrace();
                }
        }
    }

    public static void unregisterEntities() {
        for (CustomEntityType type : values()) {
            try {
                ((Map) getPrivateStatic(EntityTypes.class, "d")).remove(type.getCustomClass());
                ((Map) getPrivateStatic(EntityTypes.class, "f")).remove(type
                        .getCustomClass());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        for (CustomEntityType type : values()) {
            try {
                //Unregister all bastards.
                a(type.getNmsClass(), type.getName(), type.getId());
            } catch (Exception e) {
                e.printStackTrace();
            }
            // Biomes#biomes was made private so use reflection to get it.
            BiomeBase[] biomes;
            try {
                biomes = (BiomeBase[]) getPrivateStatic(BiomeBase.class, "biomes");
            } catch (Exception exc) {
                // Unable to fetch.
                return;
            }
            for (BiomeBase biomeBase : biomes) {
                if (biomeBase == null)
                    break;

                // The list fields changed names but update the meta regardless.
                for (String field : new String[] { "at", "au", "av", "aw" })
                    try {
                        Field list = BiomeBase.class.getDeclaredField(field);
                        list.setAccessible(true);
                        @SuppressWarnings("unchecked")
                        List<BiomeBase.BiomeMeta> mobList = (List<BiomeBase.BiomeMeta>) list
                                .get(biomeBase);

                        // Make sure the NMS class is written back over our custom
                        // class.
                        for (BiomeBase.BiomeMeta meta : mobList)
                            for (CustomEntityType entity : values())
                                if (entity.getCustomClass().equals(meta.b))
                                    meta.b = entity.getNmsClass();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }
        }
    }

    private static Object getPrivateStatic(Class clazz, String f) throws Exception {
        Field field = clazz.getDeclaredField(f);
        field.setAccessible(true);
        return field.get(null);
    }

    /**
     * Bypass and write the maps.
     */
    @SuppressWarnings( { "unchecked", "rawtypes"})
    private static void a(Class classParam, String string, int paramInt) {
        try {
            ((Map) getPrivateStatic(EntityTypes.class, "c")).put(string, classParam);
            ((Map) getPrivateStatic(EntityTypes.class, "d")).put(classParam, string);
            ((Map) getPrivateStatic(EntityTypes.class, "e")).put(paramInt,classParam);
            ((Map) getPrivateStatic(EntityTypes.class, "f")).put(classParam,paramInt);
            ((Map) getPrivateStatic(EntityTypes.class, "g")).put(string,paramInt);
        } catch (Exception e) {
            //OOF
        }
    }
}
