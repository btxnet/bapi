/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.pets;


import net.minecraft.server.v1_8_R3.*;

import java.lang.reflect.Field;

public class Humanpet extends EntityWither {

    public Humanpet(World world) {
        super(world);
    }

    @Override
    public void e(float sideMot, float forMot) {
        if (this.passenger == null || !(this.passenger instanceof EntityHuman)) {
            super.e(sideMot, forMot);
            //   this.width = 0.5F;    // Make sure the entity can walk over half slabs, instead of jumping
            return;
        }

        this.lastYaw = this.yaw = 45;//this.passenger.yaw;
        this.pitch = this.passenger.pitch * 0.5F;

        // Set the entity's pitch, yaw, head rotation etc.
        this.b((int) this.passenger.yaw, true); //[url]https://github.com/Bukkit/mc-dev/blob/master/net/minecraft/server/Entity.java#L163-L166[/url]

    }

    @Override
    public void g(float f, float f1) {
        if (this.passenger != null && this.passenger instanceof EntityHuman) {
            this.lastYaw = this.yaw = this.passenger.yaw;
            this.pitch = this.passenger.pitch * 0.5F;
            this.setYawPitch(this.yaw, this.pitch);
            this.aK = this.aI = this.yaw;
            f = ((EntityLiving) this.passenger).aZ * 0.5F;
            f1 = ((EntityLiving) this.passenger).ba;
            if (f1 <= 0.0F) {
                f1 *= 0.25F;
            }
            Field jump = null;
            try {
                jump = EntityLiving.class.getDeclaredField("aY");
            } catch (NoSuchFieldException e1) {
                jump.setAccessible(true);
                if (jump != null) { // Wouldn't want it jumping while on the ground would we?try {
                    try {
                        if (jump.getBoolean(this.passenger)) {

                            double jumpHeight = 0.5D;
                            this.motY = jumpHeight; // Used all the time in NMS for entity jumping}

                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                    this.S = 1.0F;
                    this.aM = this.bI() * 0.1F;
                    if (!this.world.isClientSide) {
                        this.k((float) this.getAttributeInstance(GenericAttributes.MOVEMENT_SPEED).getValue());
                        super.g(f, f1);
                    }

                    this.aA = this.aB;
                    double d0 = this.locX - this.lastX;
                    double d1 = this.locZ - this.lastZ;
                    float f4 = MathHelper.sqrt(d0 * d0 + d1 * d1) * 4.0F;
                    if (f4 > 1.0F) {
                        f4 = 1.0F;
                    }

                    this.aB += (f4 - this.aB) * 0.4F;
                    this.aC += this.aB;
                } else {
                    this.S = 0.5F;
                    this.aM = 0.02F;
                    super.g(f, f1);
                }

            }
        }
    }
}
