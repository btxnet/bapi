/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.external.soundplayer;

/**
 * Created by JUSTIN on 07.04.2018.
 */
import org.bukkit.Sound;

public class Instrument {
    public static Sound getInstrument(byte instrument) {
            switch (instrument) {
                case 0:
                    return Sound.valueOf("NOTE_PIANO");
                case 1:
                    return Sound.valueOf("NOTE_BASS_GUITAR");
                case 2:
                    return Sound.valueOf("NOTE_BASS_DRUM");
                case 3:
                    return Sound.valueOf("NOTE_SNARE_DRUM");
                case 4:
                    return Sound.valueOf("NOTE_STICKS");
            }
            return Sound.valueOf("NOTE_PIANO");
    }

    public static org.bukkit.Instrument getBukkitInstrument(byte instrument) {
        switch (instrument) {
            case 0:
                return org.bukkit.Instrument.PIANO;
            case 1:
                return org.bukkit.Instrument.BASS_GUITAR;
            case 2:
                return org.bukkit.Instrument.BASS_DRUM;
            case 3:
                return org.bukkit.Instrument.SNARE_DRUM;
            case 4:
                return org.bukkit.Instrument.STICKS;
        }
        return org.bukkit.Instrument.PIANO;
    }
}