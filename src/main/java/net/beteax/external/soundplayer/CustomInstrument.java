/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.external.soundplayer;

import org.bukkit.Sound;

/**
 * Created by JUSTIN on 07.04.2018.
 */
public class CustomInstrument
{
    private byte index;
    private String name;
    private String soundfile;
    private byte pitch;
    private byte press;
    private Sound sound;

    public CustomInstrument(byte index, String name, String soundfile, byte pitch, byte press)
    {
        this.index = index;
        this.name = name;
        this.soundfile = soundfile.replaceAll(".ogg", "");
        if (this.soundfile.equalsIgnoreCase("pling")) {

                    this.sound = Sound.valueOf("NOTE_PLING");

        }
        this.pitch = pitch;
        this.press = press;
    }

    public byte getIndex()
    {
        return this.index;
    }

    public String getName()
    {
        return this.name;
    }

    public String getSoundfile()
    {
        return this.soundfile;
    }

    public Sound getSound()
    {
        return this.sound;
    }
}
