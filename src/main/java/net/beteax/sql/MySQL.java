/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.sql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public abstract class MySQL {

    public abstract void connect();

    public abstract void disconnect();

    /**
     * Getter for property 'connected'.
     *
     * @return Value for property 'connected'.
     */
    public abstract boolean isConnected();

    public abstract PreparedStatement getStatement(String sql);

    public abstract ResultSet getResult(String qry);

    public abstract void update(String qry);
}
