/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax;

import net.beteax.external.soundplayer.SongPlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;

public class NoteAPI {
    public static  HashMap<String, ArrayList<SongPlayer>> playingSongs = new HashMap<>();
    private static HashMap<String, Byte> playerVolume = new HashMap<>();
    public static boolean isReceivingSong(Player player) { return ((playingSongs.get(player.getName()) != null) && (!playingSongs.get(player.getName()).isEmpty()));}
    public static void setPlayerVolume(Player player, byte volume) { playerVolume.put(player.getName(), volume); }

    public static void stopPlaying(Player player) {
        if (playingSongs.get(player.getName()) == null) {
            return;
        }
        for (SongPlayer s : playingSongs.get(player.getName())) {
            s.removePlayer(player);
        }
    }

    public static byte getPlayerVolume(Player player) {
        Byte b = playerVolume.get(player.getName());
        if (b == null) {
            b = 100;
            playerVolume.put(player.getName(), b);
        }
        return b;
    }
}
