/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax;

import com.google.gson.Gson;
import net.beteax.channels.MainframeChannel;
import net.beteax.command.DeveloperCommandi;
import net.beteax.command.FixCommand;
import net.beteax.command.KickallCommand;
import net.beteax.command.TPCommand;
import net.beteax.external.soundplayer.SongPlayer;
import net.beteax.pets.CustomEntityType;
import net.beteax.versions.WebCheck;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class BeteaxAPI extends JavaPlugin {
    public static Gson GSON = new Gson();
    private static boolean dev = true;
    private static String prefix = "";
    private static BeteaxAPI main;
    private static APIManager api;
    private boolean checkupdate = true;
    private String sqlpw = cfg.getString("MySQL.Passwort");
    private String sqldb = cfg.getString("MySQL.Datenbank");
    private String permdb =cfg.getString("Permissions.Datenbank");
    private static File sql = new File("plugins/API", "config.yml");
    public static FileConfiguration cfg = YamlConfiguration.loadConfiguration(sql);
    private boolean devmode = true;
    private WebCheck web = new WebCheck();
    private boolean isoutdated = false;
    public static BeteaxAPI getInstance(){
        return main;
    }


    @Override
    public void onEnable() {
        main = this;
        initCheck();
        try {
            cfg.save(sql);
        } catch (IOException e) {
            e.printStackTrace();
        }
        addconfig();
        api = new APIManager(this).initAPI(sqlpw,sqldb,permdb);
        if(!api.isInit()){
            System.err.println("[API]Error with API Init. ABORTING");
            try {
                wait(90000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        try {
            CustomEntityType.unregisterEntities();
            CustomEntityType.registerEntites();
        } catch (Exception e) {
            e.printStackTrace();
        }
        getCommand("devi").setExecutor(new DeveloperCommandi());
        getCommand("tp").setExecutor(new TPCommand());
        getCommand("fix").setExecutor(new FixCommand());
        getCommand("kickall").setExecutor(new KickallCommand());
        this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        this.getServer().getMessenger().registerOutgoingPluginChannel(this, "API");
        this.getServer().getMessenger().registerIncomingPluginChannel(this, "Mainframe", new MainframeChannel());
    }
    public static APIManager getAPI(){
        return api;
    }
    private void addconfig() {
        cfg.options().copyDefaults(true);
        cfg.addDefault("MySQL.Datenbank", "-");
        cfg.addDefault("MySQL.Passwort", "-");
        cfg.addDefault("Permissions.Datenbank", "-");
        cfg.addDefault("Permissions.Passwort", "-");
        //cfg.addDefault("Lobby.isLobby", false); //todo make new Lobby FLK
        try {
            cfg.save(sql);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void initCheck() {
        if (checkupdate) {
            boolean skip = false;
            if (devmode) {
                String version;
                String rev;
                rev = web.getNewestRevision();
                version = web.getNewestVersion();
                System.out.println("[Check]Newest Version: " + version + " (" + rev + ")");
                //
                rev = web.getRevisionDev();
                version = web.getVersionDev();
                System.out.println("[Check]Newest Developer Version: " + version + " (" + rev + ")");
                //
                rev = web.getRevisionLive();
                version = web.getVersionLive();
                System.out.println("[Check]Current Live Version: " + version + " (" + rev + ")");
            }
            System.out.println("[Check] Comparing Version with newest " + web.currentBuild() + " Build...");
            if (web.same(web.getRevnum(web.currentBuild()), web.getCurrentRevnum())) {
                System.out.println("[Check] You have the newest Version on Branch.");
                skip = true;
            }
            if (!skip) {
                isoutdated = true;
                Integer h = web.getCurrentRevnum();
                Integer g = web.getRevnum(web.currentBuild());
                Integer z = g - h;
                System.out.println("[Check] Update found! System is " + z + " Version(s) behind the newest Build on your Branch. Please Update..");
                System.out.println("[Check] Contact an Sysadmin for help. admin@beteaxmail.de");
            }
        }
    }
}
